import QtQuick 2.0
import Qt3D.Core 2.0
import Qt3D.Render 2.0

Entity {
    // Geometry: vertices and indices
    GeometryRenderer { // From Qt3D.Render
        id: geometry
        geometry: Geometry {
            Attribute {
                attributeType: Attribute.VertexAttribute // glVertexAttribPointer(1, 2, 3, 4, 5, 6)
                // 1. We don't need position attribute cause we have name attribute through which we have access to vertices
                name: "position"
                // 2. Size of vertex attribute
                vertexSize: 3
                // 3. Type of data
                vertexBaseType: Attribute.Float
                // 4. No normalize functions
                // 5. Stride - space between consecutive vertex attributes. Instead of stride * sizeof(float) we consider only size data size. Qt3D makes all by its own.
                count: 3
                // 6. offset. For this example offset = 0.
                byteOffset: 0

                // glBindBuffer(GL_ARRAY_BUFFER, VBO);
                // glBufferData(GL_ARRAY_BUFFER, sizeof(vertices), vertices, GL_STATIC_DRAW);
                buffer: Buffer {
                    type: Buffer.VertexBuffer
                    data: new Float32Array([
                                               -0.5, -0.5,  0.0, // Left
                                               0.5, -0.5,  0.0, // Right
                                               0.0,  0.5,  0.0 // Top
                                           ]) // Need a C++ array, Javascript Float32Array helps
                }
            }
        }
    }

    GeometryRenderer { // From Qt3D.Render
        id: geometry2
        geometry: Geometry {
            Attribute {
                attributeType: Attribute.VertexAttribute // glVertexAttribPointer(1, 2, 3, 4, 5, 6)
                name: "aPos"
                vertexSize: 3
                vertexBaseType: Attribute.Float
                count: 3
                byteOffset: 0
                byteStride: 6 * 4 // stride
                buffer: buffer
            }
            Attribute {
                attributeType: Attribute.VertexAttribute // glVertexAttribPointer(1, 2, 3, 4, 5, 6)
                name: "aColor"
                vertexSize: 3
                count: 3
                vertexBaseType: Attribute.Float
                byteOffset: 3 * 4
                byteStride: 6 * 4
                buffer: buffer
            }
        }
        Buffer {
            id: buffer
            type: Buffer.VertexBuffer
            data: new Float32Array([
                                       // positions         // colors
                                       0.5, -0.5, 0.0,  1.0, 0.0, 0.0,   // bottom right
                                       -0.5, -0.5, 0.0,  0.0, 1.0, 0.0,   // bottom left
                                       0.0,  0.5, 0.0,  0.0, 0.0, 1.0    // top
                                   ])
        }
    }

    // Timer for changing green color
    Timer {
        id: timer
        property double value: 0.0
        running: true
        repeat: true
        interval: 10
        onTriggered: {
            value += 0.01
        }
    }

    // Material
    Material {
        /* Shader uniform can be set in either:
                Material:
                    qthelp://org.qt-project.qt3d.570/qt3d/qt3drender-qmaterial.html
                Effect:
                    qthelp://org.qt-project.qt3d.570/qt3d/qt3drender-qeffect.html
                Technique:
                    qthelp://org.qt-project.qt3d.570/qt3d/qt3drender-qtechnique.html
                RenderPass:
                    qthelp://org.qt-project.qt3d.570/qt3d/qt3drender-qrenderpass.html
            But in different progress of rendering(see later examples). */
        id: material

        // Uniform parameters
        parameters: [
            Parameter {
                id: color
                name: "ourColor"
                value: Qt.rgba(0.0, greenValue, 0.0, 1.0)
                property double greenValue: Math.sin(timer.value) / 2 + 0.5
            }
        ]

        effect: Effect {
            techniques: Technique {
                graphicsApiFilter {
                    api: GraphicsApiFilter.OpenGL
                    profile: GraphicsApiFilter.CoreProfile
                    majorVersion: 3
                    minorVersion: 3
                }
                renderPasses: RenderPass {
                    renderStates: CullFace {
                        mode: CullFace.NoCulling
                    }
                    shaderProgram: ShaderProgram {
                        vertexShaderCode: loadSource(Qt.resolvedUrl("shaders/1.vert"))
                        fragmentShaderCode: loadSource(Qt.resolvedUrl("shaders/1.frag"))
                    }
                }
            }
        }
    }

    Material {
        id: material2
        parameters: [
            Parameter {name: "upsideDown"; value: false}, // set to true for Exercise 1
            Parameter {name: "hOffset"; value: 0.0} // Set to 0.5 for Exercise 2
        ]
        effect: Effect {
            techniques: Technique {
                graphicsApiFilter {
                    api: GraphicsApiFilter.OpenGL
                    profile: GraphicsApiFilter.CoreProfile
                    majorVersion: 3
                    minorVersion: 3
                }
                renderPasses: RenderPass {
                    renderStates: CullFace {
                        mode: CullFace.NoCulling
                    }
                    shaderProgram: ShaderProgram {
                        vertexShaderCode: loadSource(Qt.resolvedUrl("shaders/2.vert"))
                        fragmentShaderCode: loadSource(Qt.resolvedUrl("shaders/2.frag"))
                    }
                }
            }
        }
    }

    //Lesson1
    //    components: [material1, geometry1]
    //Lesson2, Ex.1, Ex.2.
    //Ex3 is trivial
    components: [material2, geometry2]



}
