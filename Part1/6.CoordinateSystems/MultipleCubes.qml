import QtQuick 2.0
import Qt3D.Core 2.0
import Qt3D.Input 2.0

Entity {
    id: root
    property var texture
    property var texture2
    property var projectionLens

    // We don't need to recreate geometry
    CubeGeometry {
        id: cubeGeometry
    }

    property var cubePositions: [
        Qt.vector3d( 0.0,  0.0,  0.0),
        Qt.vector3d( 2.0,  5.0, -15.0),
        Qt.vector3d(-1.5, -2.2, -2.5),
        Qt.vector3d(-3.8, -2.0, -12.3),
        Qt.vector3d( 2.4, -0.4, -3.5),
        Qt.vector3d(-1.7,  3.0, -7.5),
        Qt.vector3d( 1.3, -2.0, -2.5),
        Qt.vector3d( 1.5,  2.0, -2.5),
        Qt.vector3d( 1.5,  0.2, -1.5),
        Qt.vector3d(-1.3,  1.0, -1.5),
    ]

    // Instantiating nodes through Qt3D's repeater called NodeInstantiator
    NodeInstantiator {
        model: cubePositions
        delegate: Entity {
            CubeMaterial {
                id: cubeMaterial
                texture: root.texture
                texture2: root.texture2
                projectionLens: root.projectionLens

                startPosition: modelData // Cube position
                startAngle: 20 * index // Cube angle
                no: index
            }
            components: [cubeGeometry, cubeMaterial]
        }
    }

    /**** Another approach is to use Transform, so we don't need to create multiple materials - only add a transform to existing cube with materials ****/
//    delegate: Entity {
//        property Transform transform: Transform {
//            translation: modelData
//            rotation: fromAxisAndAngle(Qt.vector3d(0.5, 1.0, 0.0), 20 * index)
//        }
//        components: [geometry, material, transform]
//    }

}
