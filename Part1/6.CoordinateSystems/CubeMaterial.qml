import QtQuick 2.0
import Qt3D.Render 2.0

Material {
    id: material
    property var texture
    property var texture2
    property var projectionLens

    property double startAngle: 0.0
    property vector3d startPosition: Qt.vector3d(0, 0, 0)

    property int no: 0

    Timer {
        id: timer
        property double time: 0.0
        running: true
        repeat: true
        interval: 10
        onTriggered: {
            if (no%3 == 0) // Ex.3
                time += 0.01
        }
    }

    property matrix4x4 model: {
        var m = Qt.matrix4x4()
        m.rotate(timer.time % 360 * 50 + startAngle, Qt.vector3d(0.5, 1.0, 0));
        return m;
    }
    property matrix4x4 view : {
        var m = Qt.matrix4x4();
        m.translate(startPosition.plus(defaultPointOfView));
        return m;
    }

    property matrix4x4 projection: projectionLens.projectionMatrix

    parameters: [
        Parameter {name: "texture1"; value: texture},
        Parameter {name: "texture2"; value: texture2},
        Parameter {name: "model"; value: model},
        Parameter {name: "view"; value: view},
        Parameter {name: "projection"; value: projection}
    ]
    effect: Effect {
        techniques: Technique {
            graphicsApiFilter {
                api: GraphicsApiFilter.OpenGL
                profile: GraphicsApiFilter.CoreProfile
                majorVersion: 3
                minorVersion: 3
            }
            renderPasses: RenderPass {
                shaderProgram: ShaderProgram {
                    vertexShaderCode: loadSource(Qt.resolvedUrl("shaders/2.vert"))
                    fragmentShaderCode: loadSource(Qt.resolvedUrl("shaders/2.frag"))
                }
            }
        }
    }
}
