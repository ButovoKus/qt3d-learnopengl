import QtQuick 2.0
import Qt3D.Render 2.0

Material {
    id: material
    property var texture
    property var texture2
    property var projectionLens

    property matrix4x4 model: {
        var m = Qt.matrix4x4()
        m.rotate(-55, Qt.vector3d(1, 0, 0));
        return m;
    }
    property matrix4x4 view : {
        var m = Qt.matrix4x4();
        m.translate(defaultPointOfView);
        return m;
    }

    property matrix4x4 projection: {
        var fov = 45;
        var aspect = scene.width / scene.height;
        var zNear = .1;
        var zFar = 100.;
        var h = Math.tan(fov * Math.PI / 360) * zNear;
        var w = h * aspect;

        var m = Qt.matrix4x4();
        m.m11 = zNear / w;
        m.m22 = zNear / h;
        m.m33 = - (zNear + zFar) / (zFar - zNear);
        m.m34 = -2 * zNear * zFar / (zFar - zNear);
        m.m43 = -1;
        m.m44 = 0;
        return m;
    }

    parameters: [
        Parameter {name: "texture1"; value: texture},
        Parameter {name: "texture2"; value: texture2},
        Parameter {name: "model"; value: model},
        Parameter {name: "view"; value: view},
        Parameter {name: "projection"; value: projectionLens.projectionMatrix}
    ]
    effect: Effect {
        techniques: Technique {
            graphicsApiFilter {
                api: GraphicsApiFilter.OpenGL
                profile: GraphicsApiFilter.CoreProfile
                majorVersion: 3
                minorVersion: 3
            }
            renderPasses: RenderPass {
                renderStates: CullFace {
                    mode: CullFace.NoCulling
                }
                shaderProgram: ShaderProgram {
                    vertexShaderCode: loadSource(Qt.resolvedUrl("shaders/2.vert"))
                    fragmentShaderCode: loadSource(Qt.resolvedUrl("shaders/2.frag"))
                }
            }
        }
    }
}
