import QtQuick 2.9
import Qt3D.Core 2.0
import Qt3D.Render 2.0
import Qt3D.Extras 2.0
import Qt3D.Input 2.0

Entity {
    // Textures
    id: root
    property real tb: 1.0 // texture coordinate base (Ex.2 : tb = 2.0)
    property vector3d defaultPointOfView: Qt.vector3d(0.0, 0.0, -3.0)

    // Keyboard and mouse handlers for Qt3D

    TextureLoader {
        id: texture
        source: Qt.resolvedUrl("textures/container.jpg")
        generateMipMaps: true
        minificationFilter: Texture.Linear //glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR);
        magnificationFilter: Texture.Linear //glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR);
        wrapMode {
            x: WrapMode.Repeat //glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_REPEAT);
            y: WrapMode.Repeat //glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_REPEAT);
        }
    }

    TextureLoader {
        id: texture2
        source: Qt.resolvedUrl("textures/awesomeface.png")
        generateMipMaps: true
        minificationFilter: Texture.Linear //glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR);
        magnificationFilter: Texture.Linear //glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR);
        wrapMode {
            x: WrapMode.Repeat //glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_REPEAT);
            y: WrapMode.Repeat //glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_REPEAT);
        }
        format: Texture.SRGB8
    }

    // Rectangle vertices
    RectangleGeometry {
        id: rectangle
    }

    // Cube vertices
    CubeGeometry {
        id: cube
    }

    CameraLens {
        id: projectionLens
        fieldOfView: 45
        aspectRatio: scene.width / scene.height
        nearPlane: 0.1
        farPlane: 100.0
        projectionType: CameraLens.PerspectiveProjection
    }

    RectangleMaterial {
        id: rectMaterial
        texture: texture
        texture2: texture2
        projectionLens: projectionLens
    }

    CubeMaterial {
        id: cubeMaterial
        texture: texture
        texture2: texture2
        projectionLens: projectionLens
    }

    // Lesson1
//    components: [rectMaterial, rectangle]

    // Lesson2
//    components: [cubeMaterial, cube]
    // Lesson3
    MultipleCubes {
        texture: texture
        texture2: texture2
        projectionLens: projectionLens
    }

    Component.onCompleted: {
        // handling events

        // FOV on W, S
        inputHandler.upPressed.connect(function(){projectionLens.fieldOfView += 1})
        inputHandler.downPressed.connect(function(){projectionLens.fieldOfView -= 1})
        // aspect ratio on Q, E
        inputHandler.qPressed.connect(function(){projectionLens.aspectRatio += 0.1})
        inputHandler.ePressed.connect(function(){projectionLens.aspectRatio -= 0.1})
        // left/right move on A, D
        inputHandler.rightPressed.connect(function(){defaultPointOfView.x -= 1})
        inputHandler.leftPressed.connect(function(){defaultPointOfView.x += 1})
    }



}
