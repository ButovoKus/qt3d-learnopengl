import QtQuick 2.0
import Qt3D.Core 2.0
import Qt3D.Input 2.0

Entity {
    // Render loop
    CustomFramegraph {
    }

    // Implementation of lessons and examples
    WorkEntity {
    }

    // Necessary for keyboard/mouse handling
    InputSettings {
    }
}
