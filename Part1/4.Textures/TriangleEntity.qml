import QtQuick 2.0
import Qt3D.Core 2.0
import Qt3D.Render 2.0

Entity {
    // Textures
    property real tb: 1.0 // texture coordinate base (Ex.2 : tb = 2.0)
    TextureLoader {
        id: texture
        source: Qt.resolvedUrl("textures/container.jpg")
        generateMipMaps: true
        minificationFilter: Texture.Linear //glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR);
        magnificationFilter: Texture.Linear //glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR);
        wrapMode {
            x: WrapMode.Repeat //glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_REPEAT);
            y: WrapMode.Repeat //glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_REPEAT);
        }
    }

    TextureLoader {
        id: texture2
        source: Qt.resolvedUrl("textures/awesomeface.png")
        generateMipMaps: true
        minificationFilter: Texture.Linear //glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR);
        magnificationFilter: Texture.Linear //glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR);
        wrapMode {
            x: WrapMode.Repeat //glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_REPEAT);
            y: WrapMode.Repeat //glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_REPEAT);
        }
        format: Texture.SRGB8
    }

    // Geometry
    GeometryRenderer { // From Qt3D.Render
        id: geometry
        geometry: Geometry {
            id: geom
            property int byteStride: 3 * 4 + 3 * 4 + 4 * 2
            boundingVolumePositionAttribute: position // glBindBuffer(GL_ARRAY_BUFFER, 0) resolving position attributes
            Attribute {
                id: position
                attributeType: Attribute.VertexAttribute // glVertexAttribPointer(1, 2, 3, 4, 5, 6)
                name: "aPos"
                vertexSize: 3
                vertexBaseType: Attribute.Float
                count: 4
                byteOffset: 0
                byteStride: geom.byteStride // stride
                buffer: buffer
            }
            Attribute {
                attributeType: Attribute.VertexAttribute // glVertexAttribPointer(1, 2, 3, 4, 5, 6)
                name: "aColor"
                vertexSize: 3
                count: 4
                vertexBaseType: Attribute.Float
                byteOffset: 3 * 4
                byteStride: geom.byteStride
                buffer: buffer
            }
            Attribute {
                attributeType: Attribute.VertexAttribute // glVertexAttribPointer(1, 2, 3, 4, 5, 6)
                name: "aTexCoord"
                vertexSize: 2
                count: 4
                vertexBaseType: Attribute.Float
                byteOffset: 3 * 4 + 3 * 4
                byteStride: geom.byteStride
                buffer: buffer
            }
            Attribute {
                attributeType: Attribute.IndexAttribute
                vertexSize: 1
                vertexBaseType: Attribute.UnsignedShort
                count: 6
                buffer: Buffer {
                    type: Buffer.IndexBuffer
                    data: new Uint16Array([
                                              0, 1, 3,  // First Triangle
                                              1, 2, 3,   // Second Triangle
                                          ])
                }
            }
        }
        Buffer {
            id: buffer
            type: Buffer.VertexBuffer
            data: new Float32Array([
                                       // positions          // colors           // texture coords
                                       0.5,  0.5, 0.0,   1.0, 0.0, 0.0,   tb, tb,   // top right
                                       0.5, -0.5, 0.0,   0.0, 1.0, 0.0,   tb, 0.0,   // bottom right
                                       -0.5, -0.5, 0.0,   0.0, 0.0, 1.0,   0.0, 0.0,   // bottom left
                                       -0.5,  0.5, 0.0,   1.0, 1.0, 0.0,   0.0, tb    // top left
                                   ])
        }
    }

    // Material
    Material {
        id: material
        parameters: Parameter {name: "ourTexture"; value: texture}
        effect: Effect {
            techniques: Technique {
                graphicsApiFilter {
                    api: GraphicsApiFilter.OpenGL
                    profile: GraphicsApiFilter.CoreProfile
                    majorVersion: 3
                    minorVersion: 3
                }
                renderPasses: RenderPass {
                    renderStates: CullFace {
                        mode: CullFace.NoCulling
                    }
                    shaderProgram: ShaderProgram {
                        vertexShaderCode: loadSource(Qt.resolvedUrl("shaders/1.vert"))
                        fragmentShaderCode: loadSource(Qt.resolvedUrl("shaders/1.frag"))
                    }
                }
            }
        }
    }
    Material {
        id: material2
        parameters: [
            Parameter {name: "texture1"; value: texture},
            Parameter {name: "texture2"; value: texture2},
            Parameter {name: "reverse"; value: false}, // Ex.1
            Parameter {name: "mixValue"; value: 0.1} // Ex.1
        ]
        effect: Effect {
            techniques: Technique {
                graphicsApiFilter {
                    api: GraphicsApiFilter.OpenGL
                    profile: GraphicsApiFilter.CoreProfile
                    majorVersion: 3
                    minorVersion: 3
                }
                renderPasses: RenderPass {
                    renderStates: CullFace {
                        mode: CullFace.NoCulling
                    }
                    shaderProgram: ShaderProgram {
                        vertexShaderCode: loadSource(Qt.resolvedUrl("shaders/2.vert"))
                        fragmentShaderCode: loadSource(Qt.resolvedUrl("shaders/2.frag"))
                    }
                }
            }
        }
    }
    // Lesson1
//    components: [material, geometry]

    // Lesson2
    components: [material2, geometry]



}
