import QtQuick 2.0
import Qt3D.Render 2.0

Material {
    id: material
    property double radians: Math.PI / 180.0
    property var texture
    property var texture2
    property var projectionLens

    property double startAngle: 0.0
    property vector3d startPosition: Qt.vector3d(0, 0, 0)

    property int no: 0

    Timer {
        id: timer
        property double time: 0.0
        running: true
        repeat: true
        interval: 10
        onTriggered: {
            time += 0.01
        }
    }

    property real radius: 10.0
    property real camX: Math.sin(timer.time) * radius
    property real camZ: Math.cos(timer.time) * radius
    property vector3d position: Qt.vector3d(camX, 0.0, camZ)
    property vector3d target: Qt.vector3d(0.0, 0.0, 0.0)
    property vector3d up: Qt.vector3d(0.0, 1.0, 0.0)

    // Lesson2
    property vector3d cameraPos: Qt.vector3d(0.0, 0.0, 3.0)
    property vector3d cameraFront: frontVec/// Lesson2:Qt.vector3d(0.0, 0.0, -1.0)
    property vector3d cameraUp: Qt.vector3d(0.0, 1.0, 0.0)
    property vector3d cameraRight: cameraFront.crossProduct(cameraUp).normalized()
    property real cameraSpeed: 5
    property real mouseSpeed: 5

    // Lesson3
    property real pitch: 0.0
    onPitchChanged: {
        if (pitch > 89.0)
            pitch = 89.0
        if (pitch < -89.0)
            pitch = -89.0
    }

    property real yaw: -90
    property vector3d frontVec: Qt.vector3d(Math.cos(yaw * radians) * Math.cos(pitch * radians),
                                         Math.sin(pitch * radians),
                                         Math.sin(yaw * radians) * Math.cos(pitch * radians)).normalized()

    property matrix4x4 model: {
        var m = Qt.matrix4x4()
        m.translate(startPosition);
        m.rotate(timer.time % 360 * 50 + startAngle, Qt.vector3d(0.5, 1.0, 0));
        return m;
    }
    property matrix4x4 view : {
        var m = Qt.matrix4x4();
//        m.lookAt(position, target, up)
        // Lesson2
//        m.lookAt(cameraPos, cameraPos.plus(cameraFront), cameraUp)
        //Lesson 3
        m.lookAt(cameraPos, cameraPos.plus(cameraFront), cameraUp)
        return m;
    }

    property matrix4x4 projection: projectionLens.projectionMatrix

    parameters: [
        Parameter {name: "texture1"; value: texture},
        Parameter {name: "texture2"; value: texture2},
        Parameter {name: "model"; value: model},
        Parameter {name: "view"; value: view},
        Parameter {name: "projection"; value: projection}
    ]
    effect: Effect {
        techniques: Technique {
            graphicsApiFilter {
                api: GraphicsApiFilter.OpenGL
                profile: GraphicsApiFilter.CoreProfile
                majorVersion: 3
                minorVersion: 3
            }
            renderPasses: RenderPass {
                shaderProgram: ShaderProgram {
                    vertexShaderCode: loadSource(Qt.resolvedUrl("shaders/2.vert"))
                    fragmentShaderCode: loadSource(Qt.resolvedUrl("shaders/2.frag"))
                }
            }
        }
    }

    function processKeys(dt) {
        var dx = cameraSpeed * dt;
        if (inputHandler.keys.up) {
            cameraPos = cameraPos.plus(cameraFront.times(dx))
        }
        if (inputHandler.keys.down) {
            cameraPos = cameraPos.minus(cameraFront.times(dx))
        }
        if (inputHandler.keys.right) {
            cameraPos = cameraPos.plus(cameraRight.times(dx))
        }
        if (inputHandler.keys.left) {
            cameraPos = cameraPos.minus(cameraRight.times(dx))
        }
        processMouse(dt)
    }

    function processMouse(dt) {
        var p = inputHandler.delta
        console.log(p.x, p.y)
        pitch -= p.y * mouseSpeed * dt
        yaw += p.x * mouseSpeed * dt
    }

    Component.onCompleted: {
        // move forward
        frameSwapper.triggered.connect(processKeys)
    }
}
