import QtQuick 2.0
import Qt3D.Render 2.0

GeometryRenderer { // From Qt3D.Render
    id: geometry
    geometry: Geometry {
        id: geom
        property int byteStride: 3 * 4 + 4 * 2
        boundingVolumePositionAttribute: position // glBindBuffer(GL_ARRAY_BUFFER, 0) resolving position attributes
        Attribute {
            id: position
            attributeType: Attribute.VertexAttribute // glVertexAttribPointer(1, 2, 3, 4, 5, 6)
            name: "aPos"
            vertexSize: 3
            vertexBaseType: Attribute.Float
            count: 4
            byteOffset: 0
            byteStride: geom.byteStride // stride
            buffer: buffer
        }
        Attribute {
            attributeType: Attribute.VertexAttribute // glVertexAttribPointer(1, 2, 3, 4, 5, 6)
            name: "aTexCoord"
            vertexSize: 2
            count: 4
            vertexBaseType: Attribute.Float
            byteOffset: 3 * 4
            byteStride: geom.byteStride
            buffer: buffer
        }
        Attribute {
            attributeType: Attribute.IndexAttribute
            vertexSize: 1
            vertexBaseType: Attribute.UnsignedShort
            count: 6
            buffer: Buffer {
                type: Buffer.IndexBuffer
                data: new Uint16Array([
                                          0, 1, 3,  // First Triangle
                                          1, 2, 3,   // Second Triangle
                                      ])
            }
        }
    }
    Buffer {
        id: buffer
        type: Buffer.VertexBuffer
        data: new Float32Array([
                                   // positions      // texture coords
                                   0.5,   0.5, 0.0,  tb,  tb,   // top right
                                   0.5,  -0.5, 0.0,  tb,  0.0,   // bottom right
                                   -0.5, -0.5, 0.0,  0.0, 0.0,   // bottom left
                                   -0.5,  0.5, 0.0,  0.0, tb    // top left
                               ])
    }
}
