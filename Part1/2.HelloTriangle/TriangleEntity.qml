import QtQuick 2.0
import Qt3D.Core 2.0
import Qt3D.Render 2.0

Entity {
    // Geometry: vertices and indices
    GeometryRenderer { // From Qt3D.Render
        id: geometry
        geometry: Geometry {
            Attribute {
                attributeType: Attribute.VertexAttribute // glVertexAttribPointer(1, 2, 3, 4, 5, 6)
                // 1. We don't need position attribute cause we have name attribute through which we have access to vertices
                name: "position"
                // 2. Size of vertex attribute
                vertexSize: 3
                // 3. Type of data
                vertexBaseType: Attribute.Float
                // 4. No normalize functions
                // 5. Stride - space between consecutive vertex attributes. Instead of stride * sizeof(float) we consider only size data size. Qt3D makes all by its own.
                count: 3
                // 6. offset. For this example offset = 0.
                byteOffset: 0

                // glBindBuffer(GL_ARRAY_BUFFER, VBO);
                // glBufferData(GL_ARRAY_BUFFER, sizeof(vertices), vertices, GL_STATIC_DRAW);
                buffer: Buffer {
                    type: Buffer.VertexBuffer
                    data: new Float32Array([
                                               -0.5, -0.5,  0.0, // Left
                                               0.5, -0.5,  0.0, // Right
                                               0.0,  0.5,  0.0 // Top
                                           ]) // Need a C++ array, Javascript Float32Array helps
                }
            }
        }
    }

    GeometryRenderer { // From Qt3D.Render
        id: geometry2
        /*** DRAW LINES INSTEAD OF FILL ***/
//        primitiveType: GeometryRenderer.LineLoop // Currently linewidth=1 only
        geometry: Geometry {
            boundingVolumePositionAttribute: position // glBindBuffer(GL_ARRAY_BUFFER, 0) resolving position attributes
            Attribute {
                id: position
                attributeType: Attribute.VertexAttribute // glVertexAttribPointer(1, 2, 3, 4, 5, 6)
                // 1. We don't need position attribute cause we have name attribute through which we have access to vertices
                name: "position"
                // 2. Size of vertex attribute
                vertexSize: 3
                // 3. Type of data
                vertexBaseType: Attribute.Float
                // 4. No normalize functions
                // 5. Stride - space between consecutive vertex attributes. Instead of stride * sizeof(float) we consider only size data size. Qt3D makes all by its own.
                count: 4
                // 6. offset. For this example offset = 0.
                byteOffset: 0

                // glBindBuffer(GL_ARRAY_BUFFER, VBO);
                // glBufferData(GL_ARRAY_BUFFER, sizeof(vertices), vertices, GL_STATIC_DRAW);
                buffer: Buffer {
                    type: Buffer.VertexBuffer
                    data: new Float32Array([
                                               0.5,  0.5, 0.0,  // top right
                                               0.5, -0.5, 0.0,  // bottom right
                                               -0.5, -0.5, 0.0,  // bottom left
                                               -0.5,  0.5, 0.0   // top left
                                           ]) // Need a C++ array, Javascript Float32Array helps
                }
            }
            Attribute {
                attributeType: Attribute.IndexAttribute
                vertexSize: 1
                vertexBaseType: Attribute.UnsignedShort
                count: 6
                buffer: Buffer {
                    type: Buffer.IndexBuffer
                    data: new Uint16Array([
                                              0, 1, 3,  // First Triangle
                                              1, 2, 3,   // Second Triangle
                                          ])
                }
            }
        }
    }

    // Ex.1
    GeometryRenderer {
        id: geometryEx1
        geometry: Geometry {
            Attribute {
                attributeType: Attribute.VertexAttribute
                vertexBaseType: Attribute.Float
                vertexSize: 3
                count: 6
                name: "position"
                buffer: Buffer {
                    type: Buffer.VertexBuffer
                    data: new Float32Array([
                                               // First triangle
                                               -0.9, -0.5, 0.0,  // Left
                                               -0.0, -0.5, 0.0,  // Right
                                               -0.45, 0.5, 0.0,  // Top
                                               // Second triangle
                                               0.0, -0.5, 0.0,  // Left
                                               0.9, -0.5, 0.0,  // Right
                                               0.45, 0.5, 0.0,  // Top
                                           ])
                }
            }
        }
    }

    // Geometry for ex.2
    GeometryRenderer {
        id: geometryEx2_1
        geometry: Geometry {
            Attribute {
                attributeType: Attribute.VertexAttribute
                vertexBaseType: Attribute.Float
                vertexSize: 3
                count: 3
                name: "position"
                buffer: Buffer {
                    type: Buffer.VertexBuffer
                    data: new Float32Array([
                                               // First triangle
                                               -0.9, -0.5, 0.0,  // Left
                                               -0.0, -0.5, 0.0,  // Right
                                               -0.45, 0.5, 0.0,  // Top
                                           ])
                }
            }
        }
    }

    // Geometry for ex.2
    GeometryRenderer {
        id: geometryEx2_2
        geometry: Geometry {
            Attribute {
                attributeType: Attribute.VertexAttribute
                vertexBaseType: Attribute.Float
                vertexSize: 3
                count: 3
                name: "position"
                buffer: Buffer {
                    type: Buffer.VertexBuffer
                    data: new Float32Array([
                                               // First triangle
                                               0.0, -0.5, 0.0,  // Left
                                               0.9, -0.5, 0.0,  // Right
                                               0.45, 0.5, 0.0,  // Top
                                           ])
                }
            }
        }
    }

    // Material
    Material {
        id: material
        effect: Effect {
            techniques: Technique {
                graphicsApiFilter {
                    api: GraphicsApiFilter.OpenGL
                    profile: GraphicsApiFilter.CoreProfile
                    majorVersion: 3
                    minorVersion: 3
                }
                renderPasses: RenderPass {
                    renderStates: CullFace {
                        mode: CullFace.NoCulling
                    }
                    shaderProgram: ShaderProgram {
                        vertexShaderCode: loadSource(Qt.resolvedUrl("shaders/1.vert"))
                        fragmentShaderCode: loadSource(Qt.resolvedUrl("shaders/1.frag"))
                    }
                }
            }
        }
    }

    // Material fo Ex.3
    Material {
        id: material2
        effect: Effect {
            techniques: Technique {
                graphicsApiFilter {
                    api: GraphicsApiFilter.OpenGL
                    profile: GraphicsApiFilter.CoreProfile
                    majorVersion: 3
                    minorVersion: 3
                }
                renderPasses: RenderPass {
                    renderStates: CullFace {
                        mode: CullFace.NoCulling
                    }
                    shaderProgram: ShaderProgram {
                        vertexShaderCode: loadSource(Qt.resolvedUrl("shaders/1.vert"))
                        fragmentShaderCode: loadSource(Qt.resolvedUrl("shaders/2.frag"))
                    }
                }
            }
        }
    }
    //Lesson1
    components: [material, geometry]

    //Lesson2
//        components: [material, geometry2]

    //Ex.1
    //    components: [material, geometryEx1]

    // Ex.2
    // Entity {id: triangle0; components: [geometryEx2_1, material]}
    // Entity {id: triangle1; components: [geometryEx2_2, material]}

    // Ex.3
//     Entity {id: triangle0; components: [geometryEx2_1, material]}
//     Entity {id: triangle1; components: [geometryEx2_2, material2]}

}
