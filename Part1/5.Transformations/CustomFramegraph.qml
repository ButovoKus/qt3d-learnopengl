import QtQuick 2.0
import Qt3D.Core 2.0
import Qt3D.Render 2.0

RenderSettings {
    activeFrameGraph: clearBuffers

    // Exerc.1
    property var viewport: Viewport {
        RenderSurfaceSelector {
            // Default render output: window surface
        }
    }

    // Exerc.2
    property var clearBuffers: ClearBuffers {
        // ClearBuffers inherits from FrameGraphNode, as output framegraph
        buffers: ClearBuffers.ColorDepthBuffer // To clear color depth buffer
        clearColor: Qt.rgba(0.2, 0.3, 0.3, 1.0) // = glClearColor
        RenderSurfaceSelector {
            // Default render output: window surface
        }
    }

}
