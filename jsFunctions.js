function mat4FromMat3(m33) {
    return Qt.matrix4x4(m33.m11, m33.m12, m33.m13, 0.0,
                        m33.m21, m33.m22, m33.m23, 0.0,
                        m33.m31, m33.m32, m33.m33, 0.0,
                        0.0,     0.0,     0.0,     -1.0)
}
