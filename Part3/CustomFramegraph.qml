import QtQuick 2.0
import Qt3D.Core 2.0
import Qt3D.Render 2.0
import "../"

RenderSettings {
    property alias camera: cameraSelector.camera
    activeFrameGraph: ClearBuffers {
        // ClearBuffers inherits from FrameGraphNode, as output framegraph
        buffers: ClearBuffers.ColorDepthBuffer // To clear color buffer
        clearColor: Qt.rgba(0.1, 0.1, 0.1, 1.0) // = glClearColor
        // Default render output: window surface
        RenderSurfaceSelector {
            CameraSelector {
                id: cameraSelector
                camera: FPVCamera {
                }
                RenderStateSet {
                    renderStates: DepthTest {
                        depthFunction: DepthTest.Less
                    }
                }
            }
        }
    }
}
