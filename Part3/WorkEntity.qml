import Qt3D.Core 2.0
import Qt3D.Render 2.0
import Qt3D.Extras 2.0
import "../"

Entity {
    id: workEntity

    property vector3d lightPos: Qt.vector3d(2.0 * Math.sin(scene.time),
                                            0.0,
                                            2.0 * Math.cos(scene.time));

    SceneLoader {
        id: mesh
        source: "model/nanosuit.obj"
    }



    //    TextureLoaderBase {
    //        id: diffuseMap
    //        source: Qt.resolvedUrl("model/body_dif.png")
    //    }
    //    TextureLoaderBase {
    //        id: specMap
    //        source: Qt.resolvedUrl("model/body_showroom_spec.png")
    //    }
    //    TextureLoaderBase {
    //        id: nMap
    //        source: Qt.resolvedUrl("model/body_showroom_ddn.png")
    //    }

    //    NormalDiffuseSpecularMapMaterial {
    //        id: material
    //        normal: nMap
    //        diffuse: diffuseMap
    //        specular: specMap
    //    }


    Entity {
        id: composite
        components: [mesh]
    }

    DirectionalLight {
        id: directionalLigth
        worldDirection: Qt.vector3d(-0.2, -1.0, -0.3)
    }

    PointLight {
        id: pointLight
        constantAttenuation: 1.0
        linearAttenuation: 0.09
        quadraticAttenuation: 0.032
        intensity: 1.0
        color: "white"
    }

    SpotLight {
        id: flashLight
        constantAttenuation: 1.0
        linearAttenuation: 0.09
        quadraticAttenuation: 0.032
        localDirection: framegraph.camera.viewVector
        cutOffAngle: 15
        color: "white"
        intensity: 1.0
    }

    Transform {
        id: flashTransformS
        translation: framegraph.camera.position
    }

    Entity {
        id: light
        components: [directionalLigth]
    }

    Entity {
        id: lightCube
        Transform {
            id: transform
            translation: lightPos
            scale: 0.2
            //            scale3D: 0.2
        }
        components: [pointLight, transform]
    }

    Entity {
        id: flashLightEntity
        components: [flashLight, flashTransform]
    }
}
