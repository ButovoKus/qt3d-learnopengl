import Qt3D.Core 2.0
import Qt3D.Render 2.0
import Qt3D.Extras 2.0
import "../"
import "../../"

Entity {
    id: workEntity

    property color objectColor: Qt.rgba(1.0, 0.5, 0.31, 1.0);
    property color lightColor: Qt.rgba(1.0, 1.0, 1.0, 1.0)
    property vector3d lightPos: Qt.vector3d(2.0 * Math.sin(scene.time),
                                            0.0,
                                            2.0 * Math.cos(scene.time));


    TextureLoaderBase {
        id: diffuseMap
        source: Qt.resolvedUrl("textures/container2.png")
    }
    TextureLoaderBase {
        id: specularMap
        source: Qt.resolvedUrl("textures/container2_specular.png")
    }
    TextureLoaderBase {
        id: emissionMap
        source: Qt.resolvedUrl("textures/matrix.jpg")
    }

    CubeGeometryWithNormalsTextures {
        id: cubeGeometry
    }

    // This has default attribute names for positions and normals, so PhongMaterial can manipulate with it
    CubeGeometryWithNormalsTextures {
        id: cubeGeometryQt3D
        useQt3D: true // Don't forget to use Qt3D's attribute names for vertices, normals, etc if you want to use materials from Qt3D Extras
    }

    // Material for cube
    DiffuseSpecularMapMaterial {
        id: cubeMaterial
        diffuse: diffuseMap
        specular: specularMap
        shininess: 0.5
        ambient: Qt.rgba(0.2, 0.2, 0.2, 1.0)
    }

    Transform {
        id: cubeTransform
    }

    Entity {
        id: phongCube
        components: [cubeGeometryQt3D, cubeMaterial, cubeTransform]
    }

    PointLight {
        id: pointLight
        color: Qt.rgba(1.0, 1.0, 1.0, 1.0)
        intensity: 0.2
    }

    Entity {
        id: lightCube
        Transform {
            id: transform
            translation: lightPos
            scale: 0.2
            //            scale3D: 0.2
        }
        CubeMaterialBase {
            id: lightMaterial
            vertShaderUrl: Qt.resolvedUrl("shaders/cubeShader.vert")
            fragShaderUrl: Qt.resolvedUrl("shaders/lightShader.frag")
        }
        components: [cubeGeometry, pointLight, lightMaterial, transform]
    }
}
