import Qt3D.Core 2.0
import Qt3D.Render 2.0
import "../"
import "../../"

Entity {
    id: workEntity

    property color objectColor: Qt.rgba(1.0, 0.5, 0.31, 1.0);
    property color lightColor: Qt.rgba(1.0, 1.0, 1.0, 1.0)
    property vector3d lightPos: Qt.vector3d(1.2 * Math.sin(scene.time),
                                            1.0 * Math.sin(scene.time + 0.5),
                                            2.0 * Math.cos(scene.time));

    TextureLoaderBase {
        id: diffuseMap
        source: Qt.resolvedUrl("textures/container2.png")
    }
    TextureLoaderBase {
        id: specularMap
        source: Qt.resolvedUrl("textures/container2_specular.png")
    }
    TextureLoaderBase {
        id: emissionMap
        source: Qt.resolvedUrl("textures/matrix.jpg")
    }


    CubeGeometryWithNormalsTextures {
        id: geometry
    }

    CubeMaterialBase {
        id: diffuseMapMaterial
        parameters: [
            Parameter {name: "material.diffuse"; value: diffuseMap},
            Parameter {name: "material.specular"; value: Qt.vector3d(0.5, 0.5, 0.5)},
            Parameter {name: "material.shininess"; value: 32.0},
            Parameter {name: "light.ambient"; value: Qt.vector3d(0.1, 0.1, 0.1)},
            Parameter {name: "light.diffuse"; value: Qt.vector3d(0.5, 0.5, 0.5)},
            Parameter {name: "light.specular"; value: Qt.vector3d(1.0, 1.0, 1.0)},
            Parameter {name: "light.position"; value: lightPos},
            Parameter {name: "viewPos"; value: framegraph.camera.position}
        ]
        vertShaderUrl: Qt.resolvedUrl("shaders/cubeShader.vert")
        fragShaderUrl: Qt.resolvedUrl("shaders/cubeShaderDiffuse.frag")
    }

    CubeMaterialBase {
        id: diffuseSpecularMapMaterial
        parameters: [
            Parameter {name: "material.diffuse"; value: diffuseMap},
            Parameter {name: "material.specular"; value: specularMap},
            Parameter {name: "material.emission"; value: emissionMap},
            Parameter {name: "material.shininess"; value: 32.0},
            Parameter {name: "light.ambient"; value: Qt.vector3d(0.1, 0.1, 0.1)},
            Parameter {name: "light.diffuse"; value: Qt.vector3d(0.5, 0.5, 0.5)},
            Parameter {name: "light.specular"; value: Qt.vector3d(1.0, 1.0, 1.0)},
            Parameter {name: "light.position"; value: lightPos},
            Parameter {name: "viewPos"; value: framegraph.camera.position}
        ]
        vertShaderUrl: Qt.resolvedUrl("shaders/cubeShader.vert")
        fragShaderUrl: Qt.resolvedUrl("shaders/cubeShaderDiffuseSpecular.frag")
    }

    Transform {
        id: diffuseMapTransform
        translation: Qt.vector3d(-0.6, 0, 0)
    }

    Entity {
        id: phongCube
        components: [geometry, diffuseMapMaterial, diffuseMapTransform]
    }

    Transform {
        id: diffuseSpecularMapTransform
        translation: Qt.vector3d(0.6, 0, 0)
    }

    Entity {
        id: phongCube2
        components: [geometry, diffuseSpecularMapMaterial, diffuseSpecularMapTransform]
    }

    Entity {
        id: lightCube
        CubeMaterialBase {
            id: lightMaterial
            vertShaderUrl: Qt.resolvedUrl("shaders/cubeShader.vert")
            fragShaderUrl: Qt.resolvedUrl("shaders/lightShader.frag")
        }
        Transform {
            id: transform
            translation: lightPos
            scale: 0.2
//            scale3D: 0.2
        }

        components: [geometry, lightMaterial, transform]
    }
}
