import Qt3D.Core 2.0
import Qt3D.Render 2.0

Material {
    property string vertShaderUrl
    property string fragShaderUrl

    effect: Effect {
        techniques: Technique {
            graphicsApiFilter {
                api: GraphicsApiFilter.OpenGL
                profile: GraphicsApiFilter.CoreProfile
                majorVersion: 3
                minorVersion: 3
            }
            renderPasses: RenderPass {
                shaderProgram: ShaderProgram {
                    vertexShaderCode: loadSource(vertShaderUrl)
                    fragmentShaderCode: loadSource(fragShaderUrl)
                }
            }
        }
    }
}
