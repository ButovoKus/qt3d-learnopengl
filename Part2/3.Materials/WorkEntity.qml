import Qt3D.Core 2.0
import Qt3D.Render 2.0
import "../"

Entity {
    id: workEntity

    property color objectColor: Qt.rgba(1.0, 0.5, 0.31, 1.0);
    property color lightColor: Qt.rgba(1.0, 1.0, 1.0, 1.0)
    property vector3d lightPos: Qt.vector3d(1.2 * Math.sin(scene.time),
                                            1.0 * Math.sin(scene.time + 0.5),
                                            2.0 * Math.cos(scene.time));

    CubeGeometryWithNormals {
        id: geometry
    }

    Entity {
        id: phongCube
        CubeMaterialBase {
            id: material
            parameters: [
                Parameter {name: "material.ambient"; value: Qt.vector3d(objectColor.r, objectColor.g, objectColor.b)},
                Parameter {name: "material.diffuse"; value: Qt.vector3d(objectColor.r, objectColor.g, objectColor.b)},
                Parameter {name: "material.specular"; value: Qt.vector3d(0.5, 0.5, 0.5)},
                Parameter {name: "material.shininess"; value: 32.0},
                Parameter {name: "light.ambient"; value: Qt.vector3d(0.1, 0.1, 0.1)},
                Parameter {name: "light.diffuse"; value: Qt.vector3d(0.5, 0.5, 0.5)},
                Parameter {name: "light.specular"; value: Qt.vector3d(1.0, 1.0, 1.0)},
                Parameter {name: "light.position"; value: lightPos},
                Parameter {name: "viewPos"; value: framegraph.camera.position}
            ]
            vertShaderUrl: Qt.resolvedUrl("shaders/cubeShader.vert")
            fragShaderUrl: Qt.resolvedUrl("shaders/cubeShader.frag")
        }
        components: [geometry, material]
    }

    Entity {
        id: lightCube
        CubeMaterialBase {
            id: lightMaterial
            vertShaderUrl: Qt.resolvedUrl("shaders/cubeShader.vert")
            fragShaderUrl: Qt.resolvedUrl("shaders/lightShader.frag")
        }
        Transform {
            id: transform
            translation: lightPos
            scale: 0.2
//            scale3D: 0.2
        }

        components: [geometry, lightMaterial, transform]
    }
}
