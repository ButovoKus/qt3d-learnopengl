import Qt3D.Core 2.0
import Qt3D.Render 2.0
import Qt3D.Extras 2.0
import "../"

Entity {
    id: workEntity

    property color objectColor: Qt.rgba(1.0, 0.5, 0.31, 1.0);
    property color lightColor: Qt.rgba(1.0, 1.0, 1.0, 1.0)
    property vector3d lightPos: Qt.vector3d(1.2 * Math.sin(scene.time),
                                            1.0 * Math.sin(scene.time + 0.5),
                                            2.0 * Math.cos(scene.time));


    CubeGeometryWithNormals {
        id: cubeGeometry
    }

    // This has default attribute names for positions and normals, so PhongMaterial can manipulate with it
    CubeGeometryWithNormalsQt3D {
        id: cubeGeometryQt3D
    }

    PhongMaterial {
        id: cubeMaterial
        ambient: Qt.rgba(objectColor.r * 0.2, objectColor.g * 0.2, objectColor.b * 0.2, 1.0)
        diffuse: Qt.rgba(objectColor.r * 0.5, objectColor.g * 0.5, objectColor.b * 0.5, 1.0)
        specular: Qt.rgba(0.1, 0.1, 0.1, 1.0)
        shininess: 32.0
    }

    Transform {
        id: cubeTransform
    }

    Entity {
        id: phongCube
        components: [cubeGeometryQt3D, cubeMaterial, cubeTransform]
    }

    PointLight {
        id: pointLight
        color: Qt.rgba(1.0, 1.0, 1.0, 1.0)
        intensity: 0.5
    }

    Entity {
        id: lightCube
        Transform {
            id: transform
            translation: lightPos
            scale: 0.2
            //            scale3D: 0.2
        }
        CubeMaterialBase {
            id: lightMaterial
            vertShaderUrl: Qt.resolvedUrl("shaders/cubeShader.vert")
            fragShaderUrl: Qt.resolvedUrl("shaders/lightShader.frag")
        }
        components: [cubeGeometry, pointLight, lightMaterial, transform]
    }
}
