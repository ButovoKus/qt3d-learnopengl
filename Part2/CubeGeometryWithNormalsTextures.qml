import QtQuick 2.0
import Qt3D.Render 2.0

GeometryRenderer { // From Qt3D.Render
    id: root
    property bool useQt3D: false
    geometry: Geometry {
        id: geom
        property int byteStride: 3 * 4 + 3 * 4 + 2 * 4
        boundingVolumePositionAttribute: position // glBindBuffer(GL_ARRAY_BUFFER, 0) resolving position attributes
        Attribute {
            id: position
            attributeType: Attribute.VertexAttribute // glVertexAttribPointer(1, 2, 3, 4, 5, 6)
            name: useQt3D ? defaultPositionAttributeName : "aPos"
            vertexSize: 3
            vertexBaseType: Attribute.Float
            count: 36
            byteOffset: 0
            byteStride: geom.byteStride // stride
            buffer: buffer
        }
        Attribute {
            attributeType: Attribute.VertexAttribute // glVertexAttribPointer(1, 2, 3, 4, 5, 6)
            name: useQt3D ? defaultNormalAttributeName : "aNormal"
            vertexSize: 3
            count: 36
            vertexBaseType: Attribute.Float
            byteOffset: 3 * 4
            byteStride: geom.byteStride
            buffer: buffer
        }
        Attribute {
            attributeType: Attribute.VertexAttribute // glVertexAttribPointer(1, 2, 3, 4, 5, 6)
            name: useQt3D ? defaultTextureCoordinateAttributeName : "aTexCoords"
            vertexSize: 2
            count: 36
            vertexBaseType: Attribute.Float
            byteOffset: 3 * 4 + 3 * 4
            byteStride: geom.byteStride
            buffer: buffer
        }
    }
    Buffer {
        id: buffer
        type: Buffer.VertexBuffer
        data: new Float32Array([
                                   // positions       // normals        // texture coords
                                   -0.5, -0.5, -0.5,  0.0,  0.0, -1.0,  0.0,  0.0,
                                   0.5, -0.5, -0.5,  0.0,  0.0, -1.0,  1.0,  0.0,
                                   0.5,  0.5, -0.5,  0.0,  0.0, -1.0,  1.0,  1.0,
                                   0.5,  0.5, -0.5,  0.0,  0.0, -1.0,  1.0,  1.0,
                                   -0.5,  0.5, -0.5,  0.0,  0.0, -1.0,  0.0,  1.0,
                                   -0.5, -0.5, -0.5,  0.0,  0.0, -1.0,  0.0,  0.0,

                                   -0.5, -0.5,  0.5,  0.0,  0.0,  1.0,  0.0,  0.0,
                                   0.5, -0.5,  0.5,  0.0,  0.0,  1.0,  1.0,  0.0,
                                   0.5,  0.5,  0.5,  0.0,  0.0,  1.0,  1.0,  1.0,
                                   0.5,  0.5,  0.5,  0.0,  0.0,  1.0,  1.0,  1.0,
                                   -0.5,  0.5,  0.5,  0.0,  0.0,  1.0,  0.0,  1.0,
                                   -0.5, -0.5,  0.5,  0.0,  0.0,  1.0,  0.0,  0.0,

                                   -0.5,  0.5,  0.5, -1.0,  0.0,  0.0,  1.0,  0.0,
                                   -0.5,  0.5, -0.5, -1.0,  0.0,  0.0,  1.0,  1.0,
                                   -0.5, -0.5, -0.5, -1.0,  0.0,  0.0,  0.0,  1.0,
                                   -0.5, -0.5, -0.5, -1.0,  0.0,  0.0,  0.0,  1.0,
                                   -0.5, -0.5,  0.5, -1.0,  0.0,  0.0,  0.0,  0.0,
                                   -0.5,  0.5,  0.5, -1.0,  0.0,  0.0,  1.0,  0.0,

                                   0.5,  0.5,  0.5,  1.0,  0.0,  0.0,  1.0,  0.0,
                                   0.5,  0.5, -0.5,  1.0,  0.0,  0.0,  1.0,  1.0,
                                   0.5, -0.5, -0.5,  1.0,  0.0,  0.0,  0.0,  1.0,
                                   0.5, -0.5, -0.5,  1.0,  0.0,  0.0,  0.0,  1.0,
                                   0.5, -0.5,  0.5,  1.0,  0.0,  0.0,  0.0,  0.0,
                                   0.5,  0.5,  0.5,  1.0,  0.0,  0.0,  1.0,  0.0,

                                   -0.5, -0.5, -0.5,  0.0, -1.0,  0.0,  0.0,  1.0,
                                   0.5, -0.5, -0.5,  0.0, -1.0,  0.0,  1.0,  1.0,
                                   0.5, -0.5,  0.5,  0.0, -1.0,  0.0,  1.0,  0.0,
                                   0.5, -0.5,  0.5,  0.0, -1.0,  0.0,  1.0,  0.0,
                                   -0.5, -0.5,  0.5,  0.0, -1.0,  0.0,  0.0,  0.0,
                                   -0.5, -0.5, -0.5,  0.0, -1.0,  0.0,  0.0,  1.0,

                                   -0.5,  0.5, -0.5,  0.0,  1.0,  0.0,  0.0,  1.0,
                                   0.5,  0.5, -0.5,  0.0,  1.0,  0.0,  1.0,  1.0,
                                   0.5,  0.5,  0.5,  0.0,  1.0,  0.0,  1.0,  0.0,
                                   0.5,  0.5,  0.5,  0.0,  1.0,  0.0,  1.0,  0.0,
                                   -0.5,  0.5,  0.5,  0.0,  1.0,  0.0,  0.0,  0.0,
                                   -0.5,  0.5, -0.5,  0.0,  1.0,  0.0,  0.0,  1.0
                               ])
    }
}
