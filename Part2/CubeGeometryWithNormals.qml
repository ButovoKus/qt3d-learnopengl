import QtQuick 2.0
import Qt3D.Render 2.0

GeometryRenderer { // From Qt3D.Render
    id: root
    geometry: Geometry {
        id: geom
        property int byteStride: 3 * 4 + 3 * 4
        boundingVolumePositionAttribute: position // glBindBuffer(GL_ARRAY_BUFFER, 0) resolving position attributes
        Attribute {
            id: position
            attributeType: Attribute.VertexAttribute // glVertexAttribPointer(1, 2, 3, 4, 5, 6)
            name: "aPos"
            vertexSize: 3
            vertexBaseType: Attribute.Float
            count: 36
            byteOffset: 0
            byteStride: geom.byteStride // stride
            buffer: buffer
        }
        Attribute {
            attributeType: Attribute.VertexAttribute // glVertexAttribPointer(1, 2, 3, 4, 5, 6)
            name: "aNormal"
            vertexSize: 3
            count: 36
            vertexBaseType: Attribute.Float
            byteOffset: 3 * 4
            byteStride: geom.byteStride
            buffer: buffer
        }
    }
    Buffer {
        id: buffer
        type: Buffer.VertexBuffer
        data: new Float32Array([
                                   // positions      // normals
                                   -0.5, -0.5, -0.5,  0.0,  0.0, -1.0,
                                   0.5, -0.5, -0.5,   0.0,  0.0, -1.0,
                                   0.5,  0.5, -0.5,   0.0,  0.0, -1.0,
                                   0.5,  0.5, -0.5,   0.0,  0.0, -1.0,
                                   -0.5,  0.5, -0.5,  0.0,  0.0, -1.0,
                                   -0.5, -0.5, -0.5,  0.0,  0.0, -1.0,

                                   -0.5, -0.5,  0.5,  0.0,  0.0, 1.0,
                                   0.5, -0.5,  0.5,   0.0,  0.0, 1.0,
                                   0.5,  0.5,  0.5,   0.0,  0.0, 1.0,
                                   0.5,  0.5,  0.5,   0.0,  0.0, 1.0,
                                   -0.5,  0.5,  0.5,  0.0,  0.0, 1.0,
                                   -0.5, -0.5,  0.5,  0.0,  0.0, 1.0,

                                   -0.5,  0.5,  0.5, -1.0,  0.0,  0.0,
                                   -0.5,  0.5, -0.5, -1.0,  0.0,  0.0,
                                   -0.5, -0.5, -0.5, -1.0,  0.0,  0.0,
                                   -0.5, -0.5, -0.5, -1.0,  0.0,  0.0,
                                   -0.5, -0.5,  0.5, -1.0,  0.0,  0.0,
                                   -0.5,  0.5,  0.5, -1.0,  0.0,  0.0,

                                   0.5,  0.5,  0.5,  1.0,  0.0,  0.0,
                                   0.5,  0.5, -0.5,  1.0,  0.0,  0.0,
                                   0.5, -0.5, -0.5,  1.0,  0.0,  0.0,
                                   0.5, -0.5, -0.5,  1.0,  0.0,  0.0,
                                   0.5, -0.5,  0.5,  1.0,  0.0,  0.0,
                                   0.5,  0.5,  0.5,  1.0,  0.0,  0.0,

                                   -0.5, -0.5, -0.5,  0.0, -1.0,  0.0,
                                   0.5, -0.5, -0.5,   0.0, -1.0,  0.0,
                                   0.5, -0.5,  0.5,   0.0, -1.0,  0.0,
                                   0.5, -0.5,  0.5,   0.0, -1.0,  0.0,
                                   -0.5, -0.5,  0.5,  0.0, -1.0,  0.0,
                                   -0.5, -0.5, -0.5,  0.0, -1.0,  0.0,

                                   -0.5,  0.5, -0.5,  0.0,  1.0,  0.0,
                                   0.5,  0.5, -0.5,   0.0,  1.0,  0.0,
                                   0.5,  0.5,  0.5,   0.0,  1.0,  0.0,
                                   0.5,  0.5,  0.5,   0.0,  1.0,  0.0,
                                   -0.5,  0.5,  0.5,  0.0,  1.0,  0.0,
                                   -0.5,  0.5, -0.5,  0.0,  1.0,  0.0
                               ])
    }
}
