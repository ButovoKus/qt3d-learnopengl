import Qt3D.Core 2.0
import Qt3D.Render 2.0
import "../"
import "../../"

Entity {
    id: workEntity

    property color objectColor: Qt.rgba(1.0, 0.5, 0.31, 1.0);
    property color lightColor: Qt.rgba(1.0, 1.0, 1.0, 1.0)
    property vector3d lightPos: Qt.vector3d(1.2 * Math.sin(scene.time),
                                            1.0 * Math.sin(scene.time + 0.5),
                                            2.0 * Math.cos(scene.time));

    readonly property double radians: Math.PI / 180

    TextureLoaderBase {
        id: diffuseMap
        source: Qt.resolvedUrl("textures/container2.png")
    }
    TextureLoaderBase {
        id: specularMap
        source: Qt.resolvedUrl("textures/container2_specular.png")
    }
    TextureLoaderBase {
        id: emissionMap
        source: Qt.resolvedUrl("textures/matrix.jpg")
    }


    CubeGeometryWithNormalsTextures {
        id: geometry
    }

    CubeMaterialBase {
        id: directionalLightMaterial
        parameters: [
            Parameter {name: "material.diffuse"; value: diffuseMap},
            Parameter {name: "material.specular"; value: specularMap},
            Parameter {name: "material.emission"; value: emissionMap},
            Parameter {name: "material.shininess"; value: 32.0},
            Parameter {name: "light.ambient"; value: Qt.vector3d(0.1, 0.1, 0.1)},
            Parameter {name: "light.diffuse"; value: Qt.vector3d(0.5, 0.5, 0.5)},
            Parameter {name: "light.specular"; value: Qt.vector3d(1.0, 1.0, 1.0)},
            Parameter {name: "light.direction"; value: Qt.vector3d(-0.2, -1.0, -0.3)}, // Light direction
            Parameter {name: "viewPos"; value: framegraph.camera.position}
        ]
        vertShaderUrl: Qt.resolvedUrl("shaders/cubeShader.vert")
        fragShaderUrl: Qt.resolvedUrl("shaders/directionalLight.frag")
    }

    CubeMaterialBase {
        id: pointLightMaterial
        parameters: [
            Parameter {name: "material.diffuse"; value: diffuseMap},
            Parameter {name: "material.specular"; value: specularMap},
            Parameter {name: "material.emission"; value: emissionMap},
            Parameter {name: "material.shininess"; value: 32.0},
            Parameter {name: "light.ambient"; value: Qt.vector3d(0.1, 0.1, 0.1)},
            Parameter {name: "light.diffuse"; value: Qt.vector3d(0.5, 0.5, 0.5)},
            Parameter {name: "light.specular"; value: Qt.vector3d(1.0, 1.0, 1.0)},
            Parameter {name: "light.position"; value: lightPos},
            Parameter {name: "light.constant"; value: 1.0},
            Parameter {name: "light.linear"; value: 0.09},
            Parameter {name: "light.quadratic"; value: 0.032},
            Parameter {name: "viewPos"; value: framegraph.camera.position}
        ]
        vertShaderUrl: Qt.resolvedUrl("shaders/cubeShader.vert")
        fragShaderUrl: Qt.resolvedUrl("shaders/pointLight.frag")
    }

    CubeMaterialBase {
        id: flashLightMaterial
        parameters: [
            Parameter {name: "material.diffuse"; value: diffuseMap},
            Parameter {name: "material.specular"; value: specularMap},
            Parameter {name: "material.emission"; value: emissionMap},
            Parameter {name: "material.shininess"; value: 32.0},
            Parameter {name: "light.position"; value: framegraph.camera.position},
            Parameter {name: "light.direction"; value: framegraph.camera.viewVector},
            Parameter {name: "light.cutOff"; value: Math.cos(12.5 * radians)},
            Parameter {name: "light.ambient"; value: Qt.vector3d(0.1, 0.1, 0.1)},
            Parameter {name: "light.diffuse"; value: Qt.vector3d(0.5, 0.5, 0.5)},
            Parameter {name: "light.specular"; value: Qt.vector3d(1.0, 1.0, 1.0)},
            Parameter {name: "light.constant"; value: 1.0},
            Parameter {name: "light.linear"; value: 0.09},
            Parameter {name: "light.quadratic"; value: 0.032},
            Parameter {name: "viewPos"; value: framegraph.camera.position}
        ]
        vertShaderUrl: Qt.resolvedUrl("shaders/cubeShader.vert")
        fragShaderUrl: Qt.resolvedUrl("shaders/flashLight.frag")
    }

    CubeMaterialBase {
        id: flashLightMaterialSoft
        parameters: [
            Parameter {name: "material.diffuse"; value: diffuseMap},
            Parameter {name: "material.specular"; value: specularMap},
            Parameter {name: "material.emission"; value: emissionMap},
            Parameter {name: "material.shininess"; value: 32.0},
            Parameter {name: "light.position"; value: framegraph.camera.position},
            Parameter {name: "light.direction"; value: framegraph.camera.viewVector},
            Parameter {name: "light.cutOff"; value: Math.cos(12.5 * radians)},
            Parameter {name: "light.outerCutOff"; value: Math.cos(17.5 * radians)},
            Parameter {name: "light.ambient"; value: Qt.vector3d(0.1, 0.1, 0.1)},
            Parameter {name: "light.diffuse"; value: Qt.vector3d(0.5, 0.5, 0.5)},
            Parameter {name: "light.specular"; value: Qt.vector3d(1.0, 1.0, 1.0)},
            Parameter {name: "light.constant"; value: 1.0},
            Parameter {name: "light.linear"; value: 0.09},
            Parameter {name: "light.quadratic"; value: 0.032},
            Parameter {name: "viewPos"; value: framegraph.camera.position}
        ]
        vertShaderUrl: Qt.resolvedUrl("shaders/cubeShader.vert")
        fragShaderUrl: Qt.resolvedUrl("shaders/flashLightSoft.frag")
    }

    Entity {
        id: containerParty
        property var cubePositions: [
            Qt.vector3d( 0.0,  0.0,  0.0),
            Qt.vector3d( 2.0,  5.0, -15.0),
            Qt.vector3d(-1.5, -2.2, -2.5),
            Qt.vector3d(-3.8, -2.0, -12.3),
            Qt.vector3d( 2.4, -0.4, -3.5),
            Qt.vector3d(-1.7,  3.0, -7.5),
            Qt.vector3d( 1.3, -2.0, -2.5),
            Qt.vector3d( 1.5,  2.0, -2.5),
            Qt.vector3d( 1.5,  0.2, -1.5),
            Qt.vector3d(-1.3,  1.0, -1.5),
        ]
        NodeInstantiator {
            model: containerParty.cubePositions
            delegate: Entity {
                Transform {
                    id: transform
                    translation: modelData
                    rotation: fromAxisAndAngle(Qt.vector3d(0.5, 1.0, 0.0), 20 * index)
                    //                    rotationY: 360 * Math.cos(scene.time / 10) + 10 * index
                    //                    rotationZ: 360 * Math.sin(scene.time / 5) + 30 * index
                }

                // For directional light
                //                components: [geometry, directionalLightMaterial, transform]
                // For Point Light
//                components: [geometry, pointLightMaterial, transform]
                // For Flash light
                components: [geometry, flashLightMaterialSoft, transform]
            }
        }
    }

    Entity {
        id: lightCube
        CubeMaterialBase {
            id: lightMaterial
            vertShaderUrl: Qt.resolvedUrl("shaders/cubeShader.vert")
            fragShaderUrl: Qt.resolvedUrl("shaders/lightShader.frag")
        }
        Transform {
            id: transform
            translation: lightPos
            scale: 0.2
            //            scale3D: 0.2
        }

        components: [geometry, lightMaterial, transform]
    }
}
