import Qt3D.Core 2.0
import Qt3D.Render 2.0
import Qt3D.Extras 2.0
import "../"
import "../../"

Entity {
    id: workEntity

    property color objectColor: Qt.rgba(1.0, 0.5, 0.31, 1.0);
    property color lightColor: Qt.rgba(1.0, 1.0, 1.0, 1.0)
    property vector3d lightPos: Qt.vector3d(2.0 * Math.sin(scene.time),
                                            0.0,
                                            2.0 * Math.cos(scene.time));


    TextureLoaderBase {
        id: diffuseMap
        source: Qt.resolvedUrl("textures/container2.png")
    }
    TextureLoaderBase {
        id: specularMap
        source: Qt.resolvedUrl("textures/container2_specular.png")
    }
    TextureLoaderBase {
        id: emissionMap
        source: Qt.resolvedUrl("textures/matrix.jpg")
    }

//    // Geometry for light source
    CubeGeometryWithNormalsTextures {
        id: cubeGeometry
    }

    // This has default attribute names for positions and normals, so PhongMaterial can manipulate with it
    CubeGeometryWithNormalsTextures {
        id: cubeGeometryQt3D
        useQt3D: true // Don't forget to use Qt3D's attribute names for vertices, normals, etc if you want to use materials from Qt3D Extras
    }

    // Material for cube
    DiffuseSpecularMapMaterial {
        id: cubeMaterial
        diffuse: diffuseMap
        specular: specularMap
        shininess: 0.5
        ambient: Qt.rgba(0.1, 0.1, 0.1, 1.0)
    }

    Entity {
        id: containerParty
        property var cubePositions: [
            Qt.vector3d( 0.0,  0.0,  0.0),
            Qt.vector3d( 2.0,  5.0, -15.0),
            Qt.vector3d(-1.5, -2.2, -2.5),
            Qt.vector3d(-3.8, -2.0, -12.3),
            Qt.vector3d( 2.4, -0.4, -3.5),
            Qt.vector3d(-1.7,  3.0, -7.5),
            Qt.vector3d( 1.3, -2.0, -2.5),
            Qt.vector3d( 1.5,  2.0, -2.5),
            Qt.vector3d( 1.5,  0.2, -1.5),
            Qt.vector3d(-1.3,  1.0, -1.5),
        ]
        NodeInstantiator {
            model: containerParty.cubePositions
            delegate: Entity {
                Transform {
                    id: transform
                    translation: modelData
                    rotation: fromAxisAndAngle(Qt.vector3d(0.5, 1.0, 0.0), 20 * index)
                }
                components: [cubeGeometryQt3D, cubeMaterial, transform]
            }
        }
    }

    DirectionalLight {
        id: directionalLigth
        worldDirection: Qt.vector3d(-0.2, -1.0, -0.3)
    }

    PointLight {
        id: pointLight
        constantAttenuation: 1.0
        linearAttenuation: 0.09
        quadraticAttenuation: 0.032
        intensity: 1.0
        color: "white"
    }

    SpotLight {
        id: flashLight
        constantAttenuation: 1.0
        linearAttenuation: 0.09
        quadraticAttenuation: 0.032
        localDirection: framegraph.camera.viewVector
        cutOffAngle: 15
        color: "white"
        intensity: 1.0
    }

    Transform {
        id: flashTransform
        translation: framegraph.camera.position
    }

//    Entity {
//        id: light
//        components: [directionalLigth]
//    }

//    PointLight {
//        id: pointLight
//        color: Qt.rgba(1.0, 1.0, 1.0, 1.0)
//        intensity: 0.2
//    }

//    Entity {
//        id: lightCube
//        Transform {
//            id: transform
//            translation: lightPos
//            scale: 0.2
//            //            scale3D: 0.2
//        }
//        CubeMaterialBase {
//            id: lightMaterial
//            vertShaderUrl: Qt.resolvedUrl("shaders/cubeShader.vert")
//            fragShaderUrl: Qt.resolvedUrl("shaders/lightShader.frag")
//        }
//        components: [cubeGeometry, pointLight, lightMaterial, transform]
//    }

    Entity {
        id: flashLightEntity
        components: [flashLight, flashTransform]
    }
}
