import Qt3D.Core 2.0
import Qt3D.Render 2.0
import "../"

Entity {
    id: workEntity

    property color objectColor: Qt.rgba(1.0, 0.5, 0.31, 1.0);
    property color lightColor: Qt.rgba(1.0, 1.0, 1.0, 1.0)

    CubeGeometry {
        id: geometry
    }

    Entity {
        id: phongCube
        CubeMaterialBase {
            id: material
            parameters: [
                Parameter {name: "objectColor"; value: Qt.vector3d(objectColor.r, objectColor.g, objectColor.b)},
                Parameter {name: "lightColor"; value: Qt.vector3d(lightColor.r, lightColor.g, lightColor.b)}
            ]
            vertShaderUrl: Qt.resolvedUrl("shaders/cubeShader.vert")
            fragShaderUrl: Qt.resolvedUrl("shaders/cubeShader.frag")
        }
        components: [geometry, material]
    }

    Entity {
        id: lightCube
        CubeMaterialBase {
            id: lightMaterial
            vertShaderUrl: Qt.resolvedUrl("shaders/cubeShader.vert")
            fragShaderUrl: Qt.resolvedUrl("shaders/lightShader.frag")
        }
        Transform {
            id: transform
            translation: Qt.vector3d(1.2, 1.0, 2.0)
            scale: 0.2
//            scale3D: 0.2
        }

        components: [geometry, lightMaterial, transform]
    }
}
