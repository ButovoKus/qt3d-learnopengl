import Qt3D.Render 2.0

Technique {
    id: technique
    graphicsApiFilter {
        api: GraphicsApiFilter.OpenGL
        profile: GraphicsApiFilter.CoreProfile
        majorVersion: 3
        minorVersion: 3
    }
}
