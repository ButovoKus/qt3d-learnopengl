import Qt3D.Core 2.0
import Qt3D.Render 2.0
import QtQuick 2.0

Material {
    id: root
    property string vertShaderUrl
    property string fragShaderUrl
    property var filterKeys: []
    property var renderStates: []

    effect: Effect {
        techniques: OpenGL33Technique {
            id: technique
            renderPasses: [
                RenderPass {
                    id: renderPassDefault
                    filterKeys: root.filterKeys
                    shaderProgram: ShaderProgram {
                        vertexShaderCode: loadSource(vertShaderUrl)
                        fragmentShaderCode: loadSource(fragShaderUrl)
                    }
                }
            ]
        }
    }
}
