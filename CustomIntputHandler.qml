import QtQuick 2.0
import Qt3D.Core 2.0
import Qt3D.Input 2.0

Entity {
    id: inputHandler
    property var keys: {
        "up": false,
                "down": false,
                "left": false,
                "right": false,
                "shift": false,
    }

    property var mouse: {
        "x": 0.0,
                "y": 0.0,
                "wheelPressed": false,
                "wheelDelta": 0.0
    }

    property point curr: Qt.point(-1.0, -1.0)
    property point last: Qt.point(-1.0, -1.0)
    property point delta: Qt.point(0.0, 0.0)

    function updatePos() {
        delta = Qt.point(curr.x - last.x, curr.y - last.y)
        mouse.wheelDelta = 0
        last = curr
    }

    signal numberPressed(var num)
    signal incPressed()
    signal decPressed()

    KeyboardDevice {
        id: keyboardDevice
    }

    KeyboardHandler {
        id: keyboardHandler
        sourceDevice: keyboardDevice
        focus: true
        onPressed: {
            switch (event.key) {
            case Qt.key_W:
            case 'W'.charCodeAt(0):
            case Qt.Key_Up:
                inputHandler.keys.up = true;
                break;
            case Qt.key_S:
            case 'S'.charCodeAt(0):
            case Qt.Key_Down:
                inputHandler.keys.down = true;
                break;
            case Qt.key_A:
            case 'A'.charCodeAt(0):
            case Qt.Key_Left:
                inputHandler.keys.left = true;
                break;
            case Qt.key_D:
            case 'D'.charCodeAt(0):
            case Qt.Key_Right:
                inputHandler.keys.right = true;
                break;
            case Qt.Key_Shift:
                inputHandler.keys.shift = true;
                break;
            case Qt.key_0:
            case "0".charCodeAt(0):
                numberPressed(0);
                break;
            case Qt.key_1:
            case "1".charCodeAt(0):
                numberPressed(1);
                break;
            case Qt.key_2:
            case "2".charCodeAt(0):
                numberPressed(2);
                break;
            case Qt.key_3:
            case "3".charCodeAt(0):
                numberPressed(3);
                break;
            case Qt.key_4:
            case "4".charCodeAt(0):
                numberPressed(4);
                break;
            case Qt.key_5:
            case "5".charCodeAt(0):
                numberPressed(5);
                break;
            case Qt.key_6:
            case "6".charCodeAt(0):
                numberPressed(6);
                break;
            case Qt.key_7:
            case "7".charCodeAt(0):
                numberPressed(7);
                break;
            case Qt.key_8:
            case "8".charCodeAt(0):
                numberPressed(8);
                break;
            case Qt.key_9:
            case "9".charCodeAt(0):
                numberPressed(9);
                break;
            case "+".charCodeAt(0):
                incPressed();
                break;
            case "-".charCodeAt(0):
                decPressed();
                break;
            }
        }
        onReleased: {
            switch (event.key) {
            case Qt.key_W:
            case 'W'.charCodeAt(0):
            case Qt.Key_Up:
                inputHandler.keys.up = false;
                break;
            case Qt.key_S:
            case 'S'.charCodeAt(0):
            case Qt.Key_Down:
                inputHandler.keys.down = false;
                break;
            case Qt.key_A:
            case 'A'.charCodeAt(0):
            case Qt.Key_Left:
                inputHandler.keys.left = false;
                break;
            case Qt.key_D:
            case 'D'.charCodeAt(0):
            case Qt.Key_Right:
                inputHandler.keys.right = false;
                break;
            case Qt.Key_Shift:
                inputHandler.keys.shift = false;
                break;
            }
        }
    }

    MouseDevice {
        id: mouseDevice
    }

    MouseHandler {
        id: mouseHandler
        sourceDevice: mouseDevice
        onPositionChanged: {
            curr = Qt.point(mouse.x, mouse.y)
            if (last.x < 0 || last.y < 0) {
                last = curr
            }
        }
        onPressed: {
            curr = Qt.point(mouse.x, mouse.y)
            last = curr
            updatePos()
            if (mouse.button === Qt.MidButton) {
                inputHandler.mouse.wheelPressed = true;
            }
        }
        onReleased: {
            if (mouse.button === Qt.MidButton) {
                inputHandler.mouse.wheelPressed = false;
            }
        }
        onWheel: {
            inputHandler.mouse.wheelDelta = wheel.angleDelta.y / 360.0
        }
    }

    Component.onCompleted: {
        frameSwapper.triggered.connect(updatePos)
    }
}
