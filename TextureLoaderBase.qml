import Qt3D.Render 2.0

TextureLoader {
    id: texture
    generateMipMaps: true
    minificationFilter: Texture.Linear //glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR);
    magnificationFilter: Texture.Linear //glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR);
    wrapMode {
        x: WrapMode.Repeat //glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_REPEAT);
        y: WrapMode.Repeat //glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_REPEAT);
    }
}
