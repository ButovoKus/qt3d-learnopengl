import QtQuick 2.10
import QtQuick.Controls 2.4
import QtQuick.Scene3D 2.0
import QtQuick.Window 2.3

import Qt3D.Logic 2.0
import Qt3D.Input 2.0

import "Part4/6.Cubemap"

/********************************************/
//uniform variables (found in renderview.cpp)

//modelMatrix
//viewMatrix
//projectionMatrix
//modelView
//viewProjectionMatrix
//modelViewProjection
//mvp
//inverseModelMatrix
//inverseViewMatrix
//inverseProjectionMatrix
//inverseModelView
//inverseViewProjectionMatrix
//inverseModelViewProjection
//modelNormalMatrix
//modelViewNormal
//viewportMatrix
//inverseViewportMatrix
//exposure
//gamma
//time
//eyePosition

/***************************************/
//attributes (found in qattribute.cpp)

//vertexPosition
//vertexNormal
//vertexColor
//vertexTexCoord
//vertexTangent

Window {
    width: 1920
    height: 1080
    title: qsTr("Tabs")

    // Creating 3D scene
    Scene3D {
        id: scene
        anchors.fill: parent
        aspects: ["input", "logic"]
        focus: true
        property double time: 0.0
//        hoverEnabled: true
        // Qt3D is entity-based system, so it consists from a tree of entities
        // First is CustomIntputHandler to handle global values of input data
        CustomIntputHandler {
            id: inputHandler
            FrameAction {
                id: frameSwapper
                onTriggered: {
                    scene.time += dt
                }
            }
            InputSettings {
            }
            RootEntity {
            }
        }
    }
    Component.onCompleted: {
        show()
    }
}
