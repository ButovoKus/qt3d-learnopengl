import QtQuick 2.0
import Qt3D.Core 2.0
import Qt3D.Render 2.0
import "../"

RenderSettings {
    id: root
    property var camera
    property Texture2D screenTexture: Texture2D {
        width: scene.width
        height: scene.height
        format: Texture.RGBA32F
        generateMipMaps: false
        magnificationFilter: Texture.Linear
        minificationFilter: Texture.Linear
        wrapMode {
            x: WrapMode.ClampToEdge
            y: WrapMode.ClampToEdge
        }
    }

    property Texture2D depthTexture: Texture2D {
        width: scene.width
        height: scene.height
        format: Texture.DepthFormat
        generateMipMaps: false
        magnificationFilter: Texture.Linear
        minificationFilter: Texture.Linear
        wrapMode {
            x: WrapMode.ClampToEdge
            y: WrapMode.ClampToEdge
        }
    }

    activeFrameGraph:
        RenderSurfaceSelector {
        RenderPassFilter {
            matchAny: FilterKey {name: "pass"; value: "forward"}
            RenderTargetSelector {
                target: RenderTarget {
                    attachments: [
                        RenderTargetOutput {
                            attachmentPoint: RenderTargetOutput.Color0
                            texture: screenTexture
                        },
                        // Attaching depth texture
                        RenderTargetOutput {
                            attachmentPoint: RenderTargetOutput.Depth
                            texture: depthTexture
                        }
                    ]
                }
                ClearBuffers {
                    buffers: ClearBuffers.ColorDepthBuffer
                    clearColor: Qt.rgba(0.1, 0.1, 0.1, 1.0)
                    CameraSelector {
                        camera: root.camera
                        RenderStateSet {
                            renderStates: [
                                DepthTest {depthFunction: DepthTest.Less}
//                                CullFace {mode: CullFace.NoCulling}
                            ]
                        }
                        //                    }
                    }
                }
            }
        }
        RenderPassFilter {
            matchAny: FilterKey {name: "pass"; value: "screen"}
            ClearBuffers {
                buffers: ClearBuffers.ColorBuffer
                clearColor: Qt.rgba(0.0, 0.0, 0.0, 1.0)
                RenderStateSet {
                    renderStates: [
                        CullFace { mode: CullFace.Back} // Culling face to enable view
                    ]
                }
            }
        }
    }
}
