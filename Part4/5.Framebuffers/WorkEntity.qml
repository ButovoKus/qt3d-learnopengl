import Qt3D.Core 2.0
import Qt3D.Render 2.0
import Qt3D.Extras 2.0
import "../"
import "../../"
import QtQuick 2.0

Entity {
    id: workEntity

    property int currentKey: 0
    property real offset: 1.0 / 300.0

    TextureLoaderBase {
        id: cubeTexture
        source: Qt.resolvedUrl("qrc:/assets/textures/container.jpg")
    }

    TextureLoaderBase {
        id: planeTexture
        source: Qt.resolvedUrl("qrc:/assets/textures/metal.png")
    }

    CubeGeometryUV {
        id: cubeGeometry
    }

    PlaneGeometryUV {
        id: planeGeomety
    }

    ScreenGeometry {
        id: screenGeometry
    }

    CubeMaterialBase {
        id: cubeMaterial
        filterKeys: FilterKey {name: "pass"; value: "forward"}
        parameters: [Parameter {name: "texture1"; value: cubeTexture}]

        vertShaderUrl: Qt.resolvedUrl("shaders/base.vert")
        fragShaderUrl: Qt.resolvedUrl("shaders/base.frag")
    }

    CubeMaterialBase {
        id: planeMaterial
        filterKeys: FilterKey {name: "pass"; value: "forward"}
        parameters: [Parameter {name: "texture1"; value: planeTexture}]

        vertShaderUrl: Qt.resolvedUrl("shaders/base.vert")
        fragShaderUrl: Qt.resolvedUrl("shaders/base.frag")
    }

    CubeMaterialBase {
        id: screenMaterial
        filterKeys: FilterKey {name: "pass"; value: "screen"}
        parameters: [
            Parameter {name: "screenTexture"; value: framegraph.screenTexture},
            Parameter {name: "ppMode"; value: workEntity.currentKey},
            Parameter {name: "offset"; value: workEntity.offset}
        ]

        vertShaderUrl: Qt.resolvedUrl("shaders/screen.vert")
        fragShaderUrl: Qt.resolvedUrl("shaders/screen.frag")
    }

    Entity {
        id: plane
        components: [planeGeomety, planeMaterial]
    }

    NodeInstantiator {
        id: cubes
        model: [
            Qt.vector3d(-1.0, 0.0, -1.0),
            Qt.vector3d(2.0, 0.0, 0.0)
        ]
        delegate: Entity {
            Transform {
                id: transform
                translation: modelData
            }
            components: [cubeGeometry, cubeMaterial, transform]
        }
    }

    Entity {
        id: screenPrint
        components: [screenGeometry, screenMaterial]
    }

    Component.onCompleted: {
        inputHandler.numberPressed.connect(function(n){workEntity.currentKey = n}) // binding keys to shader selection
        inputHandler.incPressed.connect(function(n){workEntity.offset *= 1.1})
        inputHandler.decPressed.connect(function(n){workEntity.offset *= 0.9})
    }
}
