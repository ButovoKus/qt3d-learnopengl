import Qt3D.Core 2.0
import Qt3D.Render 2.0
import Qt3D.Extras 2.0
import "../"
import "../../"

Entity {
    id: workEntity

    TextureLoaderBase {
        id: cubeTexture
        source: Qt.resolvedUrl("qrc:/assets/textures/marble.jpg")
    }

    TextureLoaderBase {
        id: planeTexture
        source: Qt.resolvedUrl("qrc:/assets/textures/metal.png")
    }

    CubeGeometryUV {
        id: cubeGeometry
    }

    PlaneGeometryUV {
        id: planeGeomety
    }

    CubeMaterialBase {
        id: cubeMaterial
        parameters: [
            Parameter {name: "texture1"; value: cubeTexture}
        ]
        vertShaderUrl: Qt.resolvedUrl("shaders/base.vert")
        fragShaderUrl: Qt.resolvedUrl("shaders/base.frag")
    }

    CubeMaterialBase {
        id: planeMaterial
        parameters: [
            Parameter {name: "texture1"; value: planeTexture}
        ]

        vertShaderUrl: Qt.resolvedUrl("shaders/base.vert")
        fragShaderUrl: Qt.resolvedUrl("shaders/base.frag")
    }

    CubeMaterialBase {
        id: depthMaterial
        parameters: [
            Parameter {name: "texture1"; value: cubeTexture}
        ]

        vertShaderUrl: Qt.resolvedUrl("shaders/base.vert")
        fragShaderUrl: Qt.resolvedUrl("shaders/depthview.frag")
    }

    // 1000 cubes
    NodeInstantiator {
        model: 1000
        delegate: Entity {
            Transform {
                id: transform
                translation: Qt.vector3d(-100 + Math.random() * 200,
                                         -100 + Math.random() * 200,
                                         -100 + Math.random() * 200)
                scale: Math.random() * 10
            }
//            components: [cubeGeometry, cubeMaterial, transform]
            components: [cubeGeometry, depthMaterial, transform]
        }
    }

    Entity {
        id: plane
        components: [planeGeomety, depthMaterial]
    }
}
