import Qt3D.Core 2.0
import Qt3D.Render 2.0
import Qt3D.Extras 2.0
import "../"
import "../../"

Entity {
    id: workEntity

    TextureLoaderBase {
        id: cubeTexture
        source: Qt.resolvedUrl("qrc:/assets/textures/marble.jpg")
    }

    TextureLoaderBase {
        id: planeTexture
        source: Qt.resolvedUrl("qrc:/assets/textures/metal.png")
    }

    CubeGeometryUV {
        id: cubeGeometry
    }

    PlaneGeometryUV {
        id: planeGeomety
    }

    Material {
        id: cubeMaterial
        effect: Effect {
            techniques: OpenGL33Technique {
                renderPasses: [
                    /*********** PASS 1 *************/
                    // First pass - draw default cubes
                    RenderPass {
                        filterKeys: FilterKey {name: "pass"; value: "material"}
                        parameters: [
                            Parameter {name: "texture1"; value: cubeTexture}
                        ]
                        shaderProgram: ShaderProgram {
                            vertexShaderCode: loadSource(Qt.resolvedUrl("shaders/base.vert"))
                            fragmentShaderCode: loadSource(Qt.resolvedUrl("shaders/base.frag"))
                        }
                    },
                    /*********** PASS 2 *************/
//                     Second pass - draw outline
                    RenderPass {
                        filterKeys: FilterKey {name: "pass"; value: "outline"}
                        parameters: [
                            Parameter {name: "width"; value: 1.1}
                        ]
                        shaderProgram: ShaderProgram {
                            vertexShaderCode: loadSource(Qt.resolvedUrl("shaders/outline.vert"))
                            fragmentShaderCode: loadSource(Qt.resolvedUrl("shaders/outline.frag"))
                        }
                    }
                ]
            }
        }
    }

    CubeMaterialBase {
        id: planeMaterial
        parameters: [
            Parameter {name: "texture1"; value: planeTexture}
        ]
        filterKeys: FilterKey {name: "pass"; value: "forward"}

        vertShaderUrl: Qt.resolvedUrl("shaders/base.vert")
        fragShaderUrl: Qt.resolvedUrl("shaders/base.frag")
    }

    CubeMaterialBase {
        id: cube1Material
        parameters: [
            Parameter {name: "texture1"; value: planeTexture}
        ]
        filterKeys: FilterKey {name: "pass"; value: "material"}
        vertShaderUrl: Qt.resolvedUrl("shaders/outline.vert")
        fragShaderUrl: Qt.resolvedUrl("shaders/outline.frag")
    }

    CubeMaterialBase {
        id: cube2Material
        parameters: [
            Parameter {name: "texture1"; value: cubeTexture}
        ]
        filterKeys: FilterKey {name: "pass"; value: "material"}
        vertShaderUrl: Qt.resolvedUrl("shaders/base.vert")
        fragShaderUrl: Qt.resolvedUrl("shaders/base.frag")
    }

    Transform {
        id: transform1
        translation: Qt.vector3d(3.0, 0.0, 0.0)
    }

    Transform {
        id: transform2
        translation: Qt.vector3d(-3.0, 0.0, 0.0)
    }

    Transform {
        id: transform3
        translation: Qt.vector3d(0.0, 0.5, 0.0)
    }

    Entity {
        id: cube1
        components: [cubeGeometry, cubeMaterial]
    }

    Entity {
        id: cube2
        components: [cubeGeometry, cube1Material, transform1]
    }

    Entity {
        id: cube3
        components: [cubeGeometry, cubeMaterial, transform2]
    }

//    // 1000 cubes
//    NodeInstantiator {
//        model: 1000
//        delegate: Entity {
//            Transform {
//                id: transform
//                translation: Qt.vector3d(-100 + Math.random() * 200,
//                                         -100 + Math.random() * 200,
//                                         -100 + Math.random() * 200)
//                scale: Math.random() * 10
//            }
//            //            components: [cubeGeometry, cubeMaterial, transform]
//            components: [cubeGeometry, cubeMaterial, transform]
//        }
//    }

    Entity {
        id: plane
        components: [planeGeomety, planeMaterial, transform3]
    }
}
