import QtQuick 2.0
import Qt3D.Core 2.0
import Qt3D.Render 2.0
import "../"

RenderSettings {
    id: root
    property var camera;
    activeFrameGraph: RenderSurfaceSelector {
        CameraSelector {
            camera: root.camera
            // Clear all buffers
            RenderPassFilter {
                ClearBuffers {
                    buffers: ClearBuffers.ColorDepthStencilBuffer
                    clearColor: Qt.rgba(0.1, 0.1, 0.1, 1.0)
                    NoDraw {}
                }
            }

            /********** PASS 1 ****************/
            // Rendering scene as usual
            RenderPassFilter {
                matchAny: FilterKey {name: "pass"; value: "material"}
                RenderStateSet {
                    renderStates: [
                        DepthTest {
                            depthFunction: DepthTest.Less
                        },
                        StencilTest {
                            front.stencilFunction: StencilTestArguments.Always
                            back.stencilFunction: StencilTestArguments.Always
                        },
                        StencilOperation {
                            front.depthTestFailureOperation: StencilOperationArguments.Replace
                            back.depthTestFailureOperation: StencilOperationArguments.Replace
                        },
                        ColorMask {
                            redMasked: false
                            greenMasked: false
                            blueMasked: false
                            alphaMasked: false
                        }
                    ]
                }
            }

            /********** PASS 2 ****************/
            // Rendering outline
            RenderPassFilter {
                matchAny: FilterKey {name: "pass"; value: "outline"}
                RenderStateSet {
                    renderStates: [
                        // Disabling depth test
                        DepthTest {
                            depthFunction: DepthTest.Greater
                        },
                        StencilTest {
                            front {
                                stencilFunction: StencilTestArguments.NotEqual
                                referenceValue: 1
                                comparisonMask: 0xff
                            }
                            back {
                                stencilFunction: StencilTestArguments.NotEqual
                                referenceValue: 1
                                comparisonMask: 0xff
                            }
                        }
                    ]
                }
            }
        }
    }
}
