#version 330 core
layout (location = 0) in vec3 aPos;
layout (location = 1) in vec2 aTexCoords;

out vec2 TexCoords;

uniform mat4 mvp;
uniform float width;

void main() {
    TexCoords = aTexCoords;
    mat4 scale = mat4(width, 0.0, 0.0, 0.0,
                      0.0, width, 0.0, 0.0,
                      0.0, 0.0, width, 0.0,
                      0.0, 0.0, 0.0, 1.0);
    vec4 pos = mvp * scale * vec4(aPos, 1.0);
    gl_Position = pos;
}
