import QtQuick 2.0
import Qt3D.Render 2.0

GeometryRenderer { // From Qt3D.Render
    id: root
    geometry: Geometry {
        id: geom
        property int byteStride: 3 * 4 + 2 * 4
        boundingVolumePositionAttribute: position // glBindBuffer(GL_ARRAY_BUFFER, 0) resolving position attributes
        Attribute {
            id: position
            attributeType: Attribute.VertexAttribute // glVertexAttribPointer(1, 2, 3, 4, 5, 6)
            name: "aPos"
            vertexSize: 3
            vertexBaseType: Attribute.Float
            count: 6
            byteOffset: 0
            byteStride: geom.byteStride
            buffer: buffer
        }
        Attribute {
            id: uv
            attributeType: Attribute.VertexAttribute // glVertexAttribPointer(1, 2, 3, 4, 5, 6)
            name: "aTexCoords"
            vertexSize: 2
            vertexBaseType: Attribute.Float
            count: 6
            byteOffset: 3 * 4
            byteStride: geom.byteStride
            buffer: buffer
        }
    }
    Buffer {
        id: buffer
        type: Buffer.VertexBuffer
        data: new Float32Array([
                                   // positions       // texture Coords (note we set these higher than 1 (together with GL_REPEAT as texture wrapping mode). this will cause the floor texture to repeat)
                                    5.0, -0.5,  5.0,  2.0, 0.0,
                                   -5.0, -0.5,  5.0,  0.0, 0.0,
                                   -5.0, -0.5, -5.0,  0.0, 2.0,

                                    5.0, -0.5,  5.0,  2.0, 0.0,
                                   -5.0, -0.5, -5.0,  0.0, 2.0,
                                    5.0, -0.5, -5.0,  2.0, 2.0
                               ])
    }
}
