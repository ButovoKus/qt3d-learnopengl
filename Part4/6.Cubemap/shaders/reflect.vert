#version 330 core
layout (location = 0) in vec3 aPos;
layout (location = 1) in vec3 aNormal;

out vec3 Normal;
out vec3 Position;

uniform mat4 modelMatrix;
uniform mat4 mvp;
uniform mat3 modelNormalMatrix; //transpose(inverse(model))

void main() {
    Normal = modelNormalMatrix * aNormal;
    Position = vec3(modelMatrix * vec4(aPos, 1.0));
    gl_Position = mvp * vec4(aPos, 1.0);
}
