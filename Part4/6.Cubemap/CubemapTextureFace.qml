import Qt3D.Render 2.0
import QtQuick 2.0

TextureImage {
    property int faceNum
    source: parent.source + "/" + parent.faceNames[faceNum] + "." + parent.extension
    Component.onCompleted: {
        console.log(source)
    }
    mirrored: false // !!! Turn mirroring off. Watch QTextureImage class description
}
