import Qt3D.Core 2.0
import Qt3D.Render 2.0
import Qt3D.Extras 2.0
import "../"
import "../../"
import QtQuick 2.0
import "../../jsFunctions.js" as JsFunctions

Entity {
    id: workEntity

    property int currentKey: 0
    property matrix4x4 viewMatrix: JsFunctions.mat4FromMat3(framegraph.camera.getViewMatrix())

    //-------------------------------------------------------------------------------------------------------------------------------
    /****************************/
    /******** TEXTURES **********/
    /****************************/
    TextureLoaderBase {
        id: cubeTexture
        source: Qt.resolvedUrl("qrc:/assets/textures/container.jpg")
    }

    TextureLoaderBase {
        id: planeTexture
        source: Qt.resolvedUrl("qrc:/assets/textures/metal.png")
    }

    // Implementation of cubemap
    CubemapTexture {
        id: cubemapTexture
        source: "qrc:/assets/textures/skybox"
    }


    //-------------------------------------------------------------------------------------------------------------------------------
    /****************************/
    /****** VERTEX DATA *********/
    /****************************/
    CubeGeometryUV {
        id: cubeGeometry
    }

    PlaneGeometryUV {
        id: planeGeomety
    }

    CubemapGeometry {
        id: cubemapGeometry
    }

    CubeGeometryNormal {
        id: cubeGeometryNormal
    }
    // Nanosuit
    SceneLoader {
        id: nanosuit
        source: "qrc:/assets/models/nanosuit/nanosuit.obj"
        onStatusChanged: {
            console.log(status)
        }
    }


    //-------------------------------------------------------------------------------------------------------------------------------
    /****************************/
    /******** MATERIALS *********/
    /****************************/
    // Wood box material
    CubeMaterialBase {
        id: cubeMaterial
        filterKeys: FilterKey {name: "pass"; value: "forward"}
        parameters: [Parameter {name: "texture1"; value: cubeTexture}]

        vertShaderUrl: Qt.resolvedUrl("shaders/base.vert")
        fragShaderUrl: Qt.resolvedUrl("shaders/base.frag")
    }

    // Reflect box material
    CubeMaterialBase {
        id: cubeMaterialReflect
        filterKeys: FilterKey {name: "pass"; value: "forward"}
        parameters: [
            Parameter {name: "skybox"; value: cubemapTexture},
            Parameter {name: "cameraPos"; value: framegraph.camera.position}
        ]

        vertShaderUrl: Qt.resolvedUrl("shaders/reflect.vert")
        fragShaderUrl: Qt.resolvedUrl("shaders/reflect.frag")
    }

    // Refract box material
    CubeMaterialBase {
        id: cubeMaterialRefract
        filterKeys: FilterKey {name: "pass"; value: "forward"}
        parameters: [
            Parameter {name: "skybox"; value: cubemapTexture},
            Parameter {name: "cameraPos"; value: framegraph.camera.position}
        ]

        vertShaderUrl: Qt.resolvedUrl("shaders/reflect.vert")
        fragShaderUrl: Qt.resolvedUrl("shaders/refract.frag")
    }

    CubeMaterialBase {
        id: planeMaterial
        filterKeys: FilterKey {name: "pass"; value: "forward"}
        parameters: [Parameter {name: "texture1"; value: planeTexture}]

        vertShaderUrl: Qt.resolvedUrl("shaders/base.vert")
        fragShaderUrl: Qt.resolvedUrl("shaders/base.frag")
    }

    CubeMaterialBase {
        id: skyboxMaterial
        filterKeys:  FilterKey {name: "pass"; value: "skybox"}
        parameters: [
            Parameter {name: "skybox"; value: cubemapTexture},
            Parameter {name: "viewMatrix"; value: viewMatrix}
        ]

        vertShaderUrl: Qt.resolvedUrl("shaders/skybox.vert")
        fragShaderUrl: Qt.resolvedUrl("shaders/skybox.frag")
    }

    //-------------------------------------------------------------------------------------------------------------------------------
    /****************************/
    /******** MATERIALS *********/
    /****************************/
    Entity {
        id: plane
        components: [planeGeomety, planeMaterial]
    }

    NodeInstantiator {
        id: cubes
        model: [
            Qt.vector3d(-1.0, 0.0, -1.0),
            Qt.vector3d(2.0, 0.0, 0.0)
        ]
        delegate: Entity {
            Transform {
                id: transform
                translation: modelData
            }
            components: {
                switch (workEntity.currentKey) {
                case 1: return [cubeGeometryNormal, cubeMaterialReflect, transform]
                case 2: return [cubeGeometryNormal, cubeMaterialRefract, transform]
                default: return [cubeGeometry, cubeMaterial, transform]
                }

            }
        }
    }

    Entity {
        id: suit
        components: [nanosuit, cubeMaterial]
    }

    Entity {
        id: skybox
        components: [cubemapGeometry, skyboxMaterial]
    }

    Component.onCompleted: {
        inputHandler.numberPressed.connect(function(n) {workEntity.currentKey = n}) // binding keys to shader selection
    }
}
