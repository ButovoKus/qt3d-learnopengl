import QtQuick 2.0
import Qt3D.Core 2.0
import Qt3D.Render 2.0
import "../"

RenderSettings {
    id: root
    property var camera
    activeFrameGraph: RenderSurfaceSelector {
        CameraSelector {
            camera: root.camera
            // Clear all buffers
            RenderPassFilter {
                matchAny: FilterKey {name: "pass"; value: "forward"}
                ClearBuffers {
                    buffers: ClearBuffers.ColorDepthBuffer
                    clearColor: Qt.rgba(0.1, 0.1, 0.1, 1.0)
                    RenderStateSet {
                        renderStates: [
                            DepthTest {
                                depthFunction: DepthTest.Less
                            }
                        ]
                    }
                }
            }
            RenderPassFilter {
                matchAny: FilterKey {name: "pass"; value: "skybox"}
                RenderStateSet {
                    renderStates: [
                        DepthTest {
                            depthFunction: DepthTest.LessOrEqual
                        },
                        CullFace {
                            mode: CullFace.NoCulling
                        }
                    ]
                }
            }
        }
    }
}
