import Qt3D.Render 2.0
import Qt3D.Core 2.0
import QtQuick 2.0

TextureCubeMap {
    id: root
    property var faceNames: [
        "right", "left", // px nx
        "top", "bottom", // py ny
        "front", "back"] // pz nz
    property string extension: "jpg"
    property string source: ""
    generateMipMaps: true
    minificationFilter: Texture.Linear //glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR);
    magnificationFilter: Texture.Linear //glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR);
    wrapMode {
        x: WrapMode.ClampToEdge
        y: WrapMode.ClampToEdge
        z: WrapMode.ClampToEdge
    }

    CubemapTextureFace {
        face: TextureCubeMap.CubeMapPositiveX
        faceNum: 0
    }

    CubemapTextureFace {
        face: TextureCubeMap.CubeMapNegativeX
        faceNum: 1
    }

    CubemapTextureFace {
        face: TextureCubeMap.CubeMapPositiveY
        faceNum: 2
    }

    CubemapTextureFace {
        face: TextureCubeMap.CubeMapNegativeY
        faceNum: 3
    }

    CubemapTextureFace {
        face: TextureCubeMap.CubeMapPositiveZ
        faceNum: 4
    }

    CubemapTextureFace {
        face: TextureCubeMap.CubeMapNegativeZ
        faceNum: 5
    }
}
