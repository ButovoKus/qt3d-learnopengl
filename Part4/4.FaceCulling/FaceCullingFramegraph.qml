import QtQuick 2.0
import Qt3D.Core 2.0
import Qt3D.Render 2.0
import "../"

RenderSettings {
    id: root
    property var camera
    activeFrameGraph: RenderSurfaceSelector {
        CameraSelector {
            camera: root.camera
            // Clear all buffers
            ClearBuffers {
                buffers: ClearBuffers.ColorDepthBuffer
                clearColor: Qt.rgba(0.1, 0.1, 0.1, 1.0)
                RenderStateSet {
                    renderStates: [
                        CullFace {
                            mode: CullFace.Back
                        },
                        FrontFace {
                            direction: FrontFace.ClockWise
                        }
                    ]
                }
            }
        }
    }
}
