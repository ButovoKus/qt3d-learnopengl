import Qt3D.Core 2.0
import Qt3D.Render 2.0
import Qt3D.Extras 2.0
import "../"
import "../../"

Entity {
    id: workEntity

    TextureLoaderBase {
        id: cubeTexture
        source: Qt.resolvedUrl("qrc:/assets/textures/uvgrid.jpg")
    }

    CubeCullFaceGeometry {
        id: cubeGeometry
    }

    CubeMaterialBase {
        id: cubeMaterial
        parameters: [
            Parameter {name: "texture1"; value: cubeTexture}
        ]
        vertShaderUrl: Qt.resolvedUrl("shaders/base.vert")
        fragShaderUrl: Qt.resolvedUrl("shaders/base.frag")
    }

    Entity {
        id: culledCube
        components: [cubeGeometry, cubeMaterial]
    }
}
