import Qt3D.Core 2.0
import Qt3D.Render 2.0
import Qt3D.Extras 2.0
import "../"
import "../../"

Entity {
    id: workEntity

    TextureLoaderBase {
        id: cubeTexture
        source: Qt.resolvedUrl("qrc:/assets/textures/marble.jpg")
    }

    TextureLoaderBase {
        id: planeTexture
        source: Qt.resolvedUrl("qrc:/assets/textures/metal.png")
    }

    TextureLoaderBase {
        id: grassTexture
        source: Qt.resolvedUrl("qrc:/assets/textures/grass.png")
        wrapMode {
            x: WrapMode.ClampToEdge //glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_REPEAT);
            y: WrapMode.ClampToEdge //glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_REPEAT);
        }
    }

    TextureLoaderBase {
        id: windowTexture
        source: Qt.resolvedUrl("qrc:/assets/textures/blending_transparent_window.png")
        wrapMode {
            x: WrapMode.ClampToEdge //glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_REPEAT);
            y: WrapMode.ClampToEdge //glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_REPEAT);
        }
    }

    CubeGeometryUV {
        id: cubeGeometry
    }

    PlaneGeometryUV {
        id: planeGeomety
    }

    GrassPlaneGeometry {
        id: grassPlaneGeometry
    }

    CubeMaterialBase {
        id: cubeMaterial
        filterKeys: FilterKey {name: "pass"; value: "forward"}
        parameters: [
            Parameter {name: "texture1"; value: cubeTexture}
        ]

        vertShaderUrl: Qt.resolvedUrl("shaders/base.vert")
        fragShaderUrl: Qt.resolvedUrl("shaders/base.frag")
    }

    CubeMaterialBase {
        id: planeMaterial
        filterKeys: FilterKey {name: "pass"; value: "forward"}
        parameters: [
            Parameter {name: "texture1"; value: planeTexture}
        ]

        vertShaderUrl: Qt.resolvedUrl("shaders/base.vert")
        fragShaderUrl: Qt.resolvedUrl("shaders/base.frag")
    }

    CubeMaterialBase {
        id: grassMaterial
        parameters: [
            Parameter {name: "texture1"; value: grassTexture}
        ]

        vertShaderUrl: Qt.resolvedUrl("shaders/grass.vert")
        fragShaderUrl: Qt.resolvedUrl("shaders/grass.frag")
    }

    Material {
        id: blendMaterial
        parameters: [
            Parameter {name: "texture1"; value: windowTexture}
        ]
        effect: Effect {
            techniques: OpenGL33Technique {
                renderPasses: [
                    RenderPass {
                        filterKeys: FilterKey {name: "pass"; value: "opaque"}
                        renderStates: [
                            BlendEquationArguments {
                                sourceRgb: BlendEquationArguments.SourceAlpha
                                destinationRgb: BlendEquationArguments.OneMinusSourceAlpha
                                sourceAlpha: BlendEquationArguments.One
                                destinationAlpha: BlendEquationArguments.One
                            }
                        ]
                        shaderProgram: ShaderProgram {
                            vertexShaderCode: loadSource(Qt.resolvedUrl("shaders/base.vert"))
                            fragmentShaderCode: loadSource(Qt.resolvedUrl("shaders/base.frag"))
                        }
                    }
                ]
            }
        }
    }

    Entity {
        id: plane
        components: [planeGeomety, planeMaterial]
    }

    NodeInstantiator {
        id: cubes
        model: [
            Qt.vector3d(-1.0, 0.0, -1.0),
            Qt.vector3d(2.0, 0.0, 0.0)
        ]
        delegate: Entity {
            Transform {
                id: transform
                translation: modelData
            }
            components: [cubeGeometry, cubeMaterial, transform]
        }
    }

    NodeInstantiator {
        id: grasses
        model: [
            Qt.vector3d(-1.5, 0.0, -0.48),
            Qt.vector3d( 1.5, 0.0,  0.51),
            Qt.vector3d( 0.0, 0.0,  0.7),
            Qt.vector3d(-0.3, 0.0, -2.3),
            Qt.vector3d( 0.5, 0.0, -0.6)
        ]
        delegate: Entity {
            Transform {
                id: transformG
                translation: modelData
            }
            components: [grassPlaneGeometry, blendMaterial, transformG]
        }
    }
}
