#version 330 core
layout (location = 0) in vec3 aPos;
layout (location = 1) in vec2 aTexCoords;

out vec2 TexCoords;

uniform mat4 mvp;
uniform float width;

void main() {
    TexCoords = aTexCoords;
    vec4 pos = mvp * vec4(aPos, 1.0);
    gl_Position = pos;
}
