import Qt3D.Render 2.0
import QtQuick 2.0

Camera {
    id: camera
    fieldOfView: 45
    aspectRatio: scene.width / scene.height
    nearPlane: 0.1
    farPlane: 100.0
    projectionType: CameraLens.PerspectiveProjection
    position: Qt.vector3d(0, 0, 5)
    viewCenter: Qt.vector3d(0, 0, 0)
    upVector: Qt.vector3d(0, 1, 0)

    property double wheelSpeed: 0.5
    property double translateSpeed: 0.01
    property double panAngle: 0.0
    property double tiltAngle: 0.0

    function updateCameraPos(dt) {
        var wheelPressed = inputHandler.mouse.wheelPressed
        var shiftPressed = inputHandler.keys.shift
        var dx = inputHandler.delta.x
        var dy = inputHandler.delta.y
        if (wheelPressed) {
            if (shiftPressed) {
                camera.translate(Qt.vector3d(-dx, dy, 0).times(translateSpeed), Camera.TranslateViewCenter)
            } else {
                camera.panAboutViewCenter(-dx)
                panAngle += dx

                var tiltAngleFuture = tiltAngle + dy
                if (tiltAngleFuture > -89 && tiltAngleFuture < 89) {
                    camera.tiltAboutViewCenter(-dy)
                    tiltAngle = tiltAngleFuture
                }
            }
        }
        var dir = camera.viewVector.normalized()
        camera.translate(Qt.vector3d(0.0, 0.0, wheelSpeed * inputHandler.mouse.wheelDelta), Camera.DontTranslateViewCenter)
        camera.upVector = Qt.vector3d(0, 1, 0)
    }

    function getViewMatrix() {
        var m = Qt.matrix4x4()
        m.lookAt(camera.position, camera.viewCenter, camera.upVector)
        return m
    }

    Component.onCompleted: {
        frameSwapper.triggered.connect(updateCameraPos)
    }
}
